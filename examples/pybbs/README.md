# Pybbs
- version 5.2.1
- github link: https://github.com/tomoya92/pybbs

### MYSQL
Requires a MYSQL database to be running. Create a user: `root`, password: `password`.
<!---You can modify this by opening pybbs.war, update the file `/WEB-INF/classes/application-dev.yml`. pybbs will create a self-named schema with required tables on startup if not present already.--->

### Fuzzer Executable
`nz.ac.wgtn.cornetto.jee.FuzzPybbs`

## Setting up pybbs for fuzzing

run fuzzer (at root): `./run-fuzzing-pybbs.sh`

### Fuzzing Web Application with HAR (HTTP Archive Format) files
Checkout for prerecord [HAR](https://u.pcloud.link/publink/show?code=kZAJNNXZtkh2W4KxT3mvb7cx87tHL8bA2fz7) files.

run with HAR `./run-fuzzing-pybbs.sh ${/path/to/har/dir}`

### Authentication
- `nz.ac.wgtn.cornetto.authenticators.PybbsAuthenticator`
- admin username&password `admin`&`123123`

### Known server issues (500)

- freemarker.core.InvalidReferenceException: https://github.com/tomoya92/pybbs/issues/160

### Known injection attacks

none

### Known XSS attacks
```
/search?keyword=
admin/comment/list?startDate=
admin/user/list?username=
admin/sensitive_word/list?word=
admin/tag/list?name=
```

https://github.com/tomoya92/pybbs/issues/158

### Obtaining and Building Instrumented Jar

pybbs is spring-boot-based. It does not use the default setup for jars, the customised setup is described in detail
here: [https://bitbucket.org/Li_Sui/pybbs-fuzz/].