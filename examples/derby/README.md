## about derby2.war

basically the same as derby.war , but schema references in web.xml removed in order to prevent 
parser (within fuzzer) making network connections that fail on the server

in the parser API , validation is switched off, but the parsers anyway make attempts to access the DTD

## Setting up derby for fuzzing
- apache-tomcat-8.5.71
- property file: campaigns/derby.properties

run fuzzer (at root): `./run-fuzzing-campaign-derby.sh`

### Fuzzing Web Application with HAR (HTTP Archive Format) files
Checkout for prerecord [HAR](https://u.pcloud.link/publink/show?code=kZAJNNXZtkh2W4KxT3mvb7cx87tHL8bA2fz7) files.

run with HAR `./run-fuzzing-campaign-derby.sh ${/path/to/har/dir}`