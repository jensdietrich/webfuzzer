## Setting up Jenkins for fuzzing

- apache-tomcat-8.5.71
- property file: campaigns/jenkins.properties

run fuzzer (at root): `./run-fuzzing-campaign-jenkins.sh`

### Fuzzing Web Application with HAR (HTTP Archive Format) files
Checkout for prerecord [HAR](https://u.pcloud.link/publink/show?code=kZAJNNXZtkh2W4KxT3mvb7cx87tHL8bA2fz7) files.

run with HAR `./run-fuzzing-campaign-jenkins.sh ${/path/to/har/dir}`

### Jenkins setup on local machine
If the Jenkins configuration directory `~/.jenkins` does not exist, Jenkins can be initialised by running Jenkins using the Jetty runner with the command:
`java -jar tools/jetty-runner-9.4.9.v20180320.jar  --path examples/jenkins/jenkins2.war /jenkins`

Note that running the fuzzer will also create the instance configuration in `~/.jenkins`

After installation, the post-installation setup wizard must be executed to unlock Jenkins and configure plugins. This can be done by visiting `http://localhost:8080/jenkins`, after running the previous command. The Setup wizard installs plugins in the `~/.jenkins/plugins` directory, and the password for the admin user is stored in the file: `~/.jenkins/secrets/initialAdminPassword` 

### Jenkins password
When fuzzing jenskins, check the log sequence generated when the server starts for warnings.

If running into authentication issues where a 302 is received, but a failed login is returned (as a `Location` header), ensure the password in the `examples/jenkins/jenkins_authentication.har` is correct. Note: there may be multiple entries.
By default, Jenkins uses the password found in the following file:
`.jenkins/secrets/initialAdminPassword`

See also: [Issue#15](https://bitbucket.org/jensdietrich/webfuzzer/issues/15/jenkins-authentication-issue)
