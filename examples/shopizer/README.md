# Shopizer
- version 2.12.0
- github link: 
https://github.com/shopizer-ecommerce/shopizer

## Setting up shopizer for fuzzing
- apache-tomcat-8.5.71
- property file: campaigns/shopizer.properties

run fuzzer (at root): `./run-fuzzing-campaign-shopizer.sh`

### Fuzzing Web Application with HAR (HTTP Archive Format) files
Checkout for prerecord [HAR](https://u.pcloud.link/publink/show?code=kZAJNNXZtkh2W4KxT3mvb7cx87tHL8bA2fz7) files.

run with HAR `./run-fuzzing-campaign-shopizer.sh ${/path/to/har/dir}`

### Authentication
- authenticator performs both customer login and admin login:
`nz.ac.wgtn.cornetto.authenticators.ShopizerAuthenticator`
- customer username&password: ``gg``&``gg``
- admin username&password: ``admin@shopizer.com``&``password``

### Known server issues (500)
To reproduce server errors requires:
- apache-tomcat-8.5.71
- jdk-1.8.0_202
- place examples/shopizer/shopizer.war under $TOMCAT_HOME/webapp/

scripts can be found under examples/shopizer/server-errors-scripts:
- MissingServletRequestParameterException: https://github.com/shopizer-ecommerce/shopizer/issues/638
- NullPointerException(com.salesmanager.core.business.exception.ServiceException) :https://github.com/shopizer-ecommerce/shopizer/issues/650
- IllegalStateException: https://github.com/shopizer-ecommerce/shopizer/issues/643
- IllegalArgumentException: https://github.com/shopizer-ecommerce/shopizer/issues/646

### Known injection attacks
none

### Known XSS attacks
see xss_shopizer.sh This attack has been patched. 
