# OpenMRS

There are two HAR files for OpenMRS. Due to the way in which OpenMRS works, they are unlikely to achieve much of anything.

Requirements:

* Deployed on Tomcat
* MySQL database active with a non-blank root password
* Addons installed in global folder

## Login

Sends a GET request with a BASE64 encoded username and password as a header (Authorization).

## Modules

Contains numerous POST requests that install and start various addons for OpenMRS.

## (Problems)

The only feature of OpenMRS that is usable through the browser is the Addons Manager.
Addons can be installed and started (see Modules), but doing so does not achieve anything.
Furthermore, all changes made are lost upon server restart.