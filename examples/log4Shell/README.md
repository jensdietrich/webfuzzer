## Setting up log4Shell for fuzzing

- apache-tomcat-8.5.71
- jdk-1.8.0_181
- log4j-2.14.1

property file: campaigns/log4Shell.properties

log4Shell source code: examples/log4Shell/log4j-vulnerability-src

run fuzzer (at root): 
```
mvn clean package
ant -buildfile run.xml -propertyfile campaigns/log4j.properties
```