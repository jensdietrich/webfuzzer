# OWASP-benchmark
- https://github.com/OWASP-Benchmark/BenchmarkJava
- version: commit/72258cb82f39e4e4b455e97f2aed2e5d37916bff

### Fuzzer Executable
`nz.ac.wgtn.cornetto.jee.FuzzOWASPBenchmark`

### Server deployment
Check out : https://bitbucket.org/Li_Sui/owaspbenchmark-fuzz

run `./runBenchmark.sh` (note that you have to run the server first)

### Setting up OWASP-benchmark for fuzzing

run `./run-fuzzing-OWASP.sh`

### Fuzzing Web Application with HAR (HTTP Archive Format) files
Checkout for prerecord [HAR](https://u.pcloud.link/publink/show?code=kZAJNNXZtkh2W4KxT3mvb7cx87tHL8bA2fz7) files.

run with HAR `./run-fuzzing-OWASP.sh ${/path/to/har/dir}`


### Obtaining and Building Instrumented Jar

OWASP-benchmark has it's own build and deploy. It does not use the default setup for jars, the customised setup is described in detail
here: [https://bitbucket.org/Li_Sui/owaspbenchmark-fuzz]
