
## Setting up structs for fuzzing
- apache-tomcat-8.5.71
- property file: campaigns/struts.properties

run fuzzer (at root): `./run-fuzzing-campaign-struts.sh`

### Fuzzing Web Application with HAR (HTTP Archive Format) files
Checkout for prerecord [HAR](https://u.pcloud.link/publink/show?code=kZAJNNXZtkh2W4KxT3mvb7cx87tHL8bA2fz7) files.

run with HAR `./run-fuzzing-campaign-struts.sh ${/path/to/har/dir}`