#!/bin/bash

## SCRIPT TO RUN FUZZING CAMPAIGNS FOR DERBBY IN BATCH MODE
NUMBER_OF_EXPERIMENTS=3
CAMPAIGN_RUNTIME=480
HARDIR=""
webapp=$TOMCAT_HOME/webapps/derby.fuzz

echo "building preparer"
cd ./war-preparer
mvn clean package
echo "building fuzzer"
cd ..
mvn clean package
workdir=$(pwd)

for ((i = 1; i <= ${NUMBER_OF_EXPERIMENTS}; i++)); do
  echo "iteration ${i}"
  if test -f "$webapp"; then
    rm -r -f "$webapp"
  fi
  cd $TOMCAT_HOME/bin/
  ./startup.sh
  sleep 30s
  cd $workdir
  if [ $# -eq 1 ]; then
    HARDIR=$1
    ant -buildfile run.xml -propertyfile campaigns/derby.properties -DcampaignLength=${CAMPAIGN_RUNTIME} -DoutputRootFolder=results-derby-${i} -DreplayHarDir=${HARDIR}
  else
    echo "running without har files"
    ant -buildfile run.xml -propertyfile campaigns/derby.properties -DcampaignLength=${CAMPAIGN_RUNTIME} -DoutputRootFolder=results-derby-${i}
  fi
  #zip results, remove folder to save space
  zip -r results/results-derby-${i}.zip results-derby-${i}
  rm results-derby-${i}/*
  rm -r results-derby-${i}
  cd $TOMCAT_HOME/bin/
  ./shutdown.sh
  sleep 30s
  # derby specific -- clean empty log folders generated in tomcat/bin
  find . -name "*.trace" -type f -delete
  find . -type d -empty -delete
  cd $workdir
  echo "iteration ${i} done"
done

echo "done !"
