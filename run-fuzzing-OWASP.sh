#!/bin/bash

## SCRIPT TO RUN FUZZING CAMPAIGNS FOR OWASP BENCHMARK IN BATCH MODE
NUMBER_OF_EXPERIMENTS=10
CAMPAIGN_RUNTIME=480
MAX_500_REPORTED=10000
MAX_XSS_REPORTED=10000
MAX_INJECTIONS_REPORTED=10000
MAX_REQUESTS=900000000
REPORT_ALL_XSS=true
SUBSTRING_MATCHING=true
HARDIR=""

echo "building project"
mvn clean package

for ((i = 1; i <= ${NUMBER_OF_EXPERIMENTS}; i++)); do
  echo "iteration ${i}"
  mvn dependency:copy-dependencies
  echo "starting fuzzer"
  if [ $# -eq 1 ]; then
    HARDIR=$1
    java -Xmx20g -cp target/cornetto.jar:target/dependency/* nz.ac.wgtn.cornetto.jee.FuzzOWASPBenchmark -replayHarDir ${HARDIR} -outputRootFolder results-OWASPBenchmark-${i} -campaignLength ${CAMPAIGN_RUNTIME} -max500Reported ${MAX_500_REPORTED} -maxXSSReported ${MAX_XSS_REPORTED} -maxInjectionsReported ${MAX_INJECTIONS_REPORTED} -trials ${MAX_REQUESTS} -requesttimeout 5000 -requestqueuesize 1000 -responsequeuesize 1000 -responseevaluationthreads 2 -requestexecutionthreads 3 -requestgenerationthreads 2 -subStringMatching ${SUBSTRING_MATCHING} -reportAllXSS ${REPORT_ALL_XSS}
  else
    echo "running without har files"
    java -Xmx20g -cp target/cornetto.jar:target/dependency/* nz.ac.wgtn.cornetto.jee.FuzzOWASPBenchmark -outputRootFolder results-OWASPBenchmark-${i} -campaignLength ${CAMPAIGN_RUNTIME} -max500Reported ${MAX_500_REPORTED} -maxXSSReported ${MAX_XSS_REPORTED} -maxInjectionsReported ${MAX_INJECTIONS_REPORTED} -trials ${MAX_REQUESTS} -requesttimeout 5000 -requestqueuesize 1000 -responsequeuesize 1000 -responseevaluationthreads 2 -requestexecutionthreads 3 -requestgenerationthreads 2 -subStringMatching ${SUBSTRING_MATCHING} -reportAllXSS ${REPORT_ALL_XSS}
  fi
  #zip results, remove folder to save space
  zip -r results/results-OWASPBenchmark-${i}.zip results-OWASPBenchmark-${i}
  rm results-OWASPBenchmark-${i}/*
  rm -r results-OWASPBenchmark-${i}
  echo "iteration ${i} done"
done
echo "done !"
