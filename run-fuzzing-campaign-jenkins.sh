#!/bin/bash

## SCRIPT TO RUN FUZZING CAMPAIGNS FOR JENKINS IN BATCH MODE
NUMBER_OF_EXPERIMENTS=3
CAMPAIGN_RUNTIME=480
HARDIR=""
webapp=$TOMCAT_HOME/webapps/jenkins.fuzz
mkdir results

echo "building preparer"
cd ./war-preparer
mvn clean package
echo "building fuzzer"
cd ..
mvn clean package

for ((i = 1; i <= ${NUMBER_OF_EXPERIMENTS}; i++)); do
  echo "iteration ${i}"
  if test -f "$webapp"; then
    rm -r -f "$webapp"
  fi
  $TOMCAT_HOME/bin/startup.sh
  sleep 30s
  if [ $# -eq 1 ]; then
    HARDIR=$1
    ant -buildfile run.xml -propertyfile campaigns/jenkins.properties -DcampaignLength=${CAMPAIGN_RUNTIME} -DoutputRootFolder=results-jenkins-${i} -DreplayHarDir=${HARDIR}
  else
    echo "running without har files"
    ant -buildfile run.xml -propertyfile campaigns/jenkins.properties -DcampaignLength=${CAMPAIGN_RUNTIME} -DoutputRootFolder=results-jenkins-${i}
  fi
  #zip results, remove folder to save space
  zip -r results/results-jenkins-${i}.zip results-jenkins-${i}
  rm results-jenkins-${i}/*
  rm -r results-jenkins-${i}
  $TOMCAT_HOME/bin/shutdown.sh
  sleep30s
  echo "iteration ${i} done"

done

echo "done !"