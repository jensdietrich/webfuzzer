package nz.ac.wgtn.cornetto.jee.instrumentation;

import nz.ac.wgtn.cornetto.jee.taint.Tainter;

/**
 * Aspect to track taint related to field stores and loads.
 * @author shawn
 */
public aspect TaintTrackingFields {



    before(Object value): set((!void && !byte && !short && !int && !long && !float && !double && !boolean && !char &&
            !byte[] && !short[] && !int[] && !long[] && !float[] && !double[] && !boolean[] && !char[]) *.*) && args(value) && !within(nz.ac.wgtn.cornetto.jee..**)
             && !cflow(execution(*.new(..))) && !within(groovy..*)
    {
       Object base = thisJoinPoint.getTarget();
       String fieldName = thisJoinPoint.toString();
       String context = thisEnclosingJoinPointStaticPart.getSignature().toLongString() + ", " + thisJoinPoint.getSourceLocation();
        if (Tainter.isTrackingOn())
            Tainter.taintByFieldStore(context, base, fieldName, value);

    }


    after() returning(Object value): get((!void && !byte && !short && !int && !long && !float && !double && !boolean && !char &&
            !byte[] && !short[] && !int[] && !long[] && !float[] && !double[] && !boolean[] && !char[]) *.*)  && !within(nz.ac.wgtn.cornetto.jee..**)
             && !cflow(execution(*.new(..))) && !within(groovy..*)
    {
        Object base = thisJoinPoint.getTarget();
        String fieldName = thisJoinPoint.toString();
        String context = thisEnclosingJoinPointStaticPart.getSignature().toLongString() + ", " + thisJoinPoint.getSourceLocation();
        if (Tainter.isTrackingOn())
            Tainter.taintByFieldLoad(context, base, fieldName, value);

    }


}
