package nz.ac.wgtn.cornetto.jee.rt;

/**
 * Constants used for the feedback mechanism.
 * @author jens dietrich
 */
public class Constants {

    public static final String FUZZING_FEEDBACK_TICKET_HEADER = "WEBFUZZ-FEEDBACK-TICKET";

    public static final String FUZZING_FEEDBACK_PATH_TOKEN = "/__fuzzing_feedback";

    public static final String TAINTED_INPUT_HEADER = "WEBFUZZ-TAINTED-VALUES";

    // to be used in path to pick up non-application invocations
    public static final String SYSTEM_INVOCATIONS_TICKET = "systeminvocations";

    // request parameter name for requests to pick up non-application invocations
    // the value is the comma-separated list of application package prefixes
    public static final String SYSTEM_INVOCATIONS_APPLICATION_PACKAGE_PREFIXES_PARAMETER = "applicationpackages";

}
