package nz.ac.wgtn.cornetto.jee.taint;

import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.Set;

/**
 * Data structure for capturing  source of taint.
 * @author shawn
 */

public class Provenance {

    private Set<Object> sources;
    private WeakReference<Object> taintedValue;
    private String desc;
    private String context;
    Enum<TaintSource> sourceType;

    public Provenance(Set<Object> sources, Enum<TaintSource> sourceType, Object taintedValue, String desc, String context) {
        this.sources = Collections.newSetFromMap(new WeakIdentityHashMap<>());
        this.sources.addAll(sources);
        this.desc = desc;
        this.context = context;
        this.taintedValue = new WeakReference<>(taintedValue);
        this.sourceType = sourceType;
    }

    public String getDesc() {
        return desc;
    }

    public String getContext() {
        return context;
    }

    public Set<Object> getSources() {
        return sources;
    }


}
