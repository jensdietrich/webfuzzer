package nz.ac.wgtn.cornetto.jee.instrumentation;

import nz.ac.wgtn.cornetto.jee.taint.Tainter;

/**
 * Aspect to track taint.
 * @author shawn
 */
public aspect  TaintTracking {

    /**
    * taint return value of all callsites if any argument is tainted
     * also, taint string arguments/return value if it is equal to any of the injected tainted strings from request
     */

    after() returning(Object returnValue) : (call(* java.lang.String.substring(..)) ||
            call(* java.lang.String.concat(..)) ||
            call(* java.lang.String.replace(..)) ||
            call(* java.lang.String.toLowerCase(..)) ||
            call(* java.lang.String.getBytes(..)) ||
            call(* java.lang.StringBuffer.append(..)) ||
            call(* java.lang.StringBuilder.append(..)) ||
            call(* java.lang.StringBuffer.delete(..)) ||
            call(* java.lang.StringBuilder.delete(..)) ||
            call(* java.lang.StringBuffer.deleteCharAt(..)) ||
            call(* java.lang.StringBuilder.deleteCharAt(..)) ||
            call(* java.lang.StringBuffer.replace(..)) ||
            call(* java.lang.StringBuilder.replace(..)) ||
            call(* java.lang.StringBuffer.substring(..)) ||
            call(* java.lang.StringBuilder.substring(..)) ||
            call(* java.lang.StringBuffer.insert(..)) ||
            call(* java.lang.StringBuilder.insert(..)) ||
            call(* java.lang.StringBuffer.reverse(..)) ||
            call(* java.lang.StringBuilder.reverse(..)) ||
            call(* java.lang.StringBuffer.reverseAllValidSurrogatePairs(..)) ||
            call(* java.lang.StringBuilder.reverseAllValidSurrogatePairs(..)) ||
            call(* java.lang.StringBuilder.toString()) ||
            call(* java.lang.StringBuffer.toString()) ||
            call(* java.lang.Class.getDeclaredMethods()) ||
            call(* java.lang.Class.getDeclaredMethod(..)) ||
            call(* java.lang.Class.getMethod(..)) ||
            call(* java.lang.Class.forName(..)) ||
            call(java.lang.String.new(..)) ||
            call(java.lang.StringBuffer.new(..)) ||
            call(java.lang.StringBuilder.new(..))) &&
            !within(nz.ac.wgtn.cornetto.jee..**)
            {
                if (Tainter.isTrackingOn()) {
                    String context = thisEnclosingJoinPointStaticPart.getSignature().toLongString() + ", " + thisJoinPoint.getSourceLocation();
                    // taint string arguments/return value if they match tainted input strings from request header
                    Tainter.introduceNewTaint(context, thisJoinPoint.getSignature().toString(), thisJoinPoint.getTarget(), thisJoinPoint.getArgs(), returnValue);
                    // taint result of invocation if any args are tainted
                    if (returnValue != null)
                        Tainter.taintByInvocation(context, thisJoinPoint.getSignature().toString(), thisJoinPoint.getTarget(), thisJoinPoint.getArgs(), returnValue);

                }
            }

            // sources
    after() returning(Object returnValue) : (call(* javax.servlet.http.HttpServletRequest.getHeader(..)) ||
            call(* javax.servlet.ServletRequest.getParameter(..))) &&
            !within(nz.ac.wgtn.cornetto.jee..**) {
        if (Tainter.isTrackingOn()) {
            String context = thisEnclosingJoinPointStaticPart.getSignature().toLongString() + ", " + thisJoinPoint.getSourceLocation();
            // taint string arguments/return value if they match tainted input strings from request header
            Tainter.introduceNewTaint(context, thisJoinPoint.getSignature().toString(), thisJoinPoint.getTarget(), thisJoinPoint.getArgs(), returnValue);
            // taint result of invocation if any args are tainted
           
        }
    }

    after() returning(Object returnValue) : (call(* java.lang.AbstractStringBuilder.getChars(..))) &&
            !within(nz.ac.wgtn.cornetto.jee..**) {
        if (Tainter.isTrackingOn()) {
            String context = thisEnclosingJoinPointStaticPart.getSignature().toLongString() + ", " + thisJoinPoint.getSourceLocation();
            Tainter.taintArgs(context, thisJoinPoint.getSignature().toString(), thisJoinPoint.getTarget(), thisJoinPoint.getArgs(), returnValue);
            // taint arguments if base is tainted

        }
    }



}
