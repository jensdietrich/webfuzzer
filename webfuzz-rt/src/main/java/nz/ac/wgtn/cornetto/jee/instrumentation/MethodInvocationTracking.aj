package nz.ac.wgtn.cornetto.jee.instrumentation;

import nz.ac.wgtn.cornetto.jee.rt.DataKind;
import nz.ac.wgtn.cornetto.jee.rt.Util;
import org.aspectj.lang.*;
import org.aspectj.lang.reflect.*;
import org.aspectj.lang.annotation.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

/**
 * Aspect to record invocations of methods.
 * @author jens dietrich
 */
public aspect MethodInvocationTracking {


    // @Before("execution(* *.*(..)) && !within(nz.ac.wgtn.cornetto.jee.**) && !within(nz.ac.wgtn.cornetto.jee.rt.**) && !within(nz.ac.wgtn.cornetto.jee.instrumentation.**) && !within(nz.ac.wgtn.cornetto.jee.**) && !within(nz.ac.wgtn.cornetto.jee.shadowed.org.json.**)" + EXCLUSIONS)
    @Before("execution(* *.*(..)) && !within(nz.ac.wgtn.cornetto.jee.rt.*) && !within(nz.ac.wgtn.cornetto.jee.instrumentation.*) && !within(nz.ac.wgtn.cornetto.jee.taint.*)")
    public void trackMethodInvocation(JoinPoint joinPoint) {
        try {
            if (joinPoint!=null) {  // spotted case of jointPoint==null in tomcat logs leading to a java.lang.ExceptionInInitializerError
                org.aspectj.lang.Signature signature = joinPoint.getSignature();
                MethodSignature methodSignature = (MethodSignature) signature;
                Method method = methodSignature.getMethod();
                String className = method.getDeclaringClass().getName();
                String methodName = method.getName();
                String descr = Util.getDescriptor(method);
                nz.ac.wgtn.cornetto.jee.rt.InvocationTracker.DEFAULT.track(DataKind.invokedMethods, className + "::" + methodName + descr);
            }
            //nz.ac.wgtn.cornetto.jee.rt.InvocationTracker.DEFAULT.track(DataKind.invokedMethods,className + "::"+ methodName + "::"+ descr);
        }
        catch (Throwable t){}
    }

//     @Before("execution(*.new(..)) && !within(nz.ac.wgtn.cornetto.jee.**) && !within(nz.ac.wgtn.cornetto.jee.rt.**) && !within(nz.ac.wgtn.cornetto.jee.instrumentation.**) && !within(nz.ac.wgtn.cornetto.jee.**) && !within(nz.ac.wgtn.cornetto.jee.shadowed.org.json.**)")
    @Before("execution(*.new(..)) && !within(nz.ac.wgtn.cornetto.jee.rt.*) && !within(nz.ac.wgtn.cornetto.jee.instrumentation.*) && !within(nz.ac.wgtn.cornetto.jee.taint.*)")
    public void trackConstructorInvocation(JoinPoint joinPoint) {
        // defensive, this causes issues with inner classes in jenkins (org.kohsuke.stapler.Function)
        try {
            if (joinPoint != null) {
                org.aspectj.lang.Signature signature = joinPoint.getSignature();
                ConstructorSignature constructorSignature = (ConstructorSignature) signature;
                Constructor constructor = constructorSignature.getConstructor();
                String className = constructor.getDeclaringClass().getName();
                String methodName = "<init>";
                String descr = Util.getDescriptor(constructor);
                nz.ac.wgtn.cornetto.jee.rt.InvocationTracker.DEFAULT.track(DataKind.invokedMethods, className + "::" + methodName + descr);
            }
        }
        catch (Throwable t){}
    }

}