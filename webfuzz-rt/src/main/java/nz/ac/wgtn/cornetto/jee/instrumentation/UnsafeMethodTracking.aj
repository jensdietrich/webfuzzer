package nz.ac.wgtn.cornetto.jee.instrumentation;

import nz.ac.wgtn.cornetto.jee.rt.DataKind;
import nz.ac.wgtn.cornetto.jee.rt.Util;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.ConstructorSignature;
import org.aspectj.lang.reflect.MethodSignature;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.List;
import nz.ac.wgtn.cornetto.jee.taint.Tainter;

/**
 * Aspect to record stacktraces of invocations of potenially unsafe methods.
 * @author jens dietrich
 */
public aspect UnsafeMethodTracking {

    /**
     * Tracking of selected system classes -- those might be unsafe sinks.
     * Callsites are instrumented, this avoid the tricky instrumentation of the actual system classes.
     * @author shawn
     * shawn - add taint checking
     */
    @Before(
    "(call(* com.opensymphony.xwork2.util.reflection.ReflectionProvider.getGetMethod(..)) || "+
    "call(* ognl.Ognl.parseExpression(..)) || "+
    "call(* com.sun.org.apache.xpath.internal.XPathAPI.selectSingleNode(..)) || "+
    "call(* javax.naming.directory.DirContext.lookup(..)) || "+
    "call(* com.sun.org.apache.xpath.internal.XPathAPI.eval(..)) || "+
    "call(* javax.xml.xpath.XPath.compile(..)) || "+
    "call(* javax.script.ScriptEngine.eval(..)) || "+
    "call(* javax.mail.Message.setSubject(..)) || "+
    "call(* javax.mail.Message.setDescription(..)) || "+
    "call(* javax.mail.Message.setDisposition(..)) || "+
    "call(* com.sun.org.apache.xpath.internal.XPathAPI.selectNodeIterator(..)) || "+
    "call(* java.sql.Statement.executeUpdate(..)) || "+
    "call(* java.lang.ProcessBuilder.command(..)) || "+
    "call(* java.sql.Statement.addBatch(..)) || "+
    "call(* com.unboundid.ldap.sdk.LDAPConnection.search(..)) || "+
    "call(* org.apache.xpath.XPathAPI.selectSingleNode(..)) || "+
    "call(* org.apache.xml.security.utils.XalanXPathAPI.selectNodeList(..)) || "+
    "call(* com.opensymphony.xwork2.util.reflection.ReflectionProvider.getSetMethod(..)) || "+
    "call(* org.springframework.ldap.core.LdapOperations.search(..)) || "+
    "call(* javax.el.ExpressionFactory.createMethodExpression(..)) || "+
    "call(* org.springframework.expression.common.TemplateAwareExpressionParser.parseExpression(..)) || "+
    "call(* javax.mail.Message.addHeader(..)) || "+
    "call(* javax.naming.directory.DirContext.search(..)) || "+
    "call(* org.apache.xml.security.utils.JDKXPathAPI.selectNodeList(..)) || "+
    "call(* com.opensymphony.xwork2.util.reflection.ReflectionProvider.getValue(..)) || "+
    "call(* com.fasterxml.jackson.databind.ObjectMapper.readValue(..)) || "+
    "call(* com.opensymphony.xwork2.util.reflection.ReflectionProvider.setProperty(..)) || "+
    "call(* com.opensymphony.xwork2.util.reflection.ReflectionProvider.setValue(..)) || "+
    "call(* org.springframework.expression.ExpressionParser.parseExpression(..)) || "+
    "call(* org.springframework.ldap.core.LdapOperations.list(..)) || "+
    "call(* java.sql.Connection.prepareStatement(..)) || "+
    "call(* javax.naming.directory.Context.lookup(..)) || "+
    "call(* javax.naming.Context.lookup(..)) || "+
    "call(* com.sun.org.apache.xpath.internal.XPathAPI.selectNodeList(..)) || "+
    "call(* com.opensymphony.xwork2.util.reflection.ReflectionProvider.getField(..)) || "+
    "call(* org.apache.xml.security.utils.XPathAPI.selectNodeList(..)) || "+
    "call(* org.springframework.expression.spel.standard.SpelExpressionParser.parseExpression(..)) || "+
    "call(* org.apache.xpath.XPathAPI.eval(..)) || "+
    "call(* java.sql.Connection.nativeSQL(..)) || "+
    "call(* java.sql.Statement.executeQuery(..)) || "+
    "call(* com.amazonaws.services.simpledb.model.SelectRequest.withSelectExpression(..)) || "+
    "call(* javax.xml.xpath.XPath.evaluate(..)) || "+
    "call(* java.lang.Runtime.exec(..)) || "+
    "call(* java.sql.Statement.execute(..)) || "+
    "call(* org.apache.xpath.XPathAPI.selectNodeIterator(..)) || "+
    "call(* org.springframework.ldap.core.LdapOperations.lookup(..)) || "+
    "call(* java.sql.Statement.executeLargeUpdate(..)) || "+
    "call(* java.sql.Connection.prepareCall(..)) || "+
    "call(* javax.el.ExpressionFactory.createValueExpression(..)) || "+
    "call(* org.apache.xpath.XPathAPI.selectNodeList(..)) || "+
    "call(* java.lang.reflect.Method.invoke(..)) || " +
    "call(java.lang.Class *..*.*ClassLoader.loadClass(..)) || " +
    "call(* java.lang.System.exit(..)) || " +
    "call(* java.io.*Stream.write(..)) || " +
    "call(* java.io.ObjectInputStream.resolveClass(..))) && " +
    "!within(nz.ac.wgtn.cornetto.jee..**) "
    )
    // NOTE that we are using Java 8 features here due to an aspectj dependency
    // TODO: port this to Java 9+ stack walker API
    public void trackUnsafeSystemSinks(JoinPoint joinPoint) {
        // System.out.println("UNSAFE SINK ENCOUNTERED: " + joinPoint);
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        List<String> encodedStackTrace = Stream.of(stackTrace).map(ste -> ste.toString()).collect(Collectors.toList());
        removeInstrumentationFramesFromStacktrace(encodedStackTrace);

        // the actual unsafe method is not yet on the stack, so this needs to be added
        Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        Method method = methodSignature.getMethod();
        String className = method.getDeclaringClass().getName();
        String methodName = method.getName();
        String descr = Util.getDescriptor(method);
        encodedStackTrace.add(0, className + '.' + methodName);
        if (Tainter.isTrackingOn()) {
            System.err.println(methodName);
            String context = joinPoint.getStaticPart().getSignature().toLongString() + ", " + joinPoint.getSourceLocation();
            // taint string arguments  if they match tainted input strings from request header
            Tainter.introduceNewTaint(context, joinPoint.getSignature().toString(), joinPoint.getTarget(), joinPoint.getArgs(), null);
            // track provenance for the tainted arguments
            Map<Integer, Object> taintedArgs = Tainter.getTaintedArgsOfSink(joinPoint.getTarget(), joinPoint.getArgs());
            if (!taintedArgs.isEmpty()) {
                Map taintInfo = new HashMap<>();
                taintInfo.put("stackTrace", encodedStackTrace);
                taintInfo.put("taintFlow", Tainter.getProvenance(taintedArgs));
                nz.ac.wgtn.cornetto.jee.rt.InvocationTracker.DEFAULT.track(DataKind.taintFlows, taintInfo);
            }
        }

        nz.ac.wgtn.cornetto.jee.rt.InvocationTracker.DEFAULT.track(DataKind.unsafeSinkInvocationStackTraces, encodedStackTrace);

        // this seems redundant, but is necessary as multiple instrumentations of the same method seem to interfer !!
        nz.ac.wgtn.cornetto.jee.rt.InvocationTracker.DEFAULT.track(DataKind.invokedMethods,className + "::"+ methodName + descr);
    }

    // filter out the instrumentation related top part of the stack
    private void removeInstrumentationFramesFromStacktrace(List<String> encodedStackTrace) {
        int instrumentationIndex = -1;
        for (int i = 0; i < encodedStackTrace.size(); i++) {
            if (encodedStackTrace.get(i).startsWith(UnsafeMethodTracking.class.getName())) {
                instrumentationIndex = i;
                break;
            }
        }
        if (instrumentationIndex > -1) {
            for (int i = 0; i <= instrumentationIndex; i++) {
                encodedStackTrace.remove(0);
            }
        }
    }

    @Before(
            "(call(java.lang.Thread.new(..)) || " +
             "call(com.amazonaws.services.simpledb.model.SelectRequest.new(..)) || "+
            "call(java.lang.ProcessBuilder.new(..))) "
    )
    public void trackUnsafeSystemSinks2(JoinPoint joinPoint) {

        // System.out.println("UNSAFE SINK ENCOUNTERED: " + joinPoint);
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        List<String> encodedStackTrace = Stream.of(stackTrace).map(ste -> ste.toString()).collect(Collectors.toList());
        removeInstrumentationFramesFromStacktrace(encodedStackTrace);

        // the actual unsafe method is not yet on the stack, so this needs to be added
        Signature signature = joinPoint.getSignature();
        ConstructorSignature methodSignature = (ConstructorSignature) signature;
        Constructor constructor = methodSignature.getConstructor();
        String className = constructor.getDeclaringClass().getName();
        String descr = Util.getDescriptor(constructor);
        encodedStackTrace.add(0, className + '.' + "<init>");

        if (Tainter.isTrackingOn()) {
            String context = joinPoint.getStaticPart().getSignature().toLongString() + ", " + joinPoint.getSourceLocation();
            // taint string arguments  if they match tainted input strings from request header
            Tainter.introduceNewTaint(context, joinPoint.getSignature().toString(), joinPoint.getTarget(), joinPoint.getArgs(), null);
            // track provenance for the tainted arguments
            Map<Integer, Object> taintedArgs = Tainter.getTaintedArgsOfSink(joinPoint.getTarget(), joinPoint.getArgs());
            if (!taintedArgs.isEmpty()) {
                Map taintInfo = new HashMap<>();
                taintInfo.put("stackTrace", encodedStackTrace);
                taintInfo.put("taintFlow", Tainter.getProvenance(taintedArgs));
                nz.ac.wgtn.cornetto.jee.rt.InvocationTracker.DEFAULT.track(DataKind.taintFlows, taintInfo);
            }
        }

        nz.ac.wgtn.cornetto.jee.rt.InvocationTracker.DEFAULT.track(DataKind.unsafeSinkInvocationStackTraces,encodedStackTrace);

        // this seems redundant, but is necessary as multiple instrumentations of the same method seem to interfer !!
        nz.ac.wgtn.cornetto.jee.rt.InvocationTracker.DEFAULT.track(DataKind.invokedMethods,className + "::"+ "<init>" + descr);

    }


}