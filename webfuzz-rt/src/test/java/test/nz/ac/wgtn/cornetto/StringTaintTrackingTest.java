package test.nz.ac.wgtn.cornetto;

import nz.ac.wgtn.cornetto.jee.taint.Tainter;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class StringTaintTrackingTest {
    @Test
    public void stringConcatTest() {
        MockHttpServletRequest req = new MockHttpServletRequest();
        req.setParameter("taintedValue", "evil");

        Set<String> taintedStrs = new HashSet<>();
        String tainted = "evil";
        taintedStrs.add(tainted);
        Tainter.initialise(taintedStrs);
        Tainter.setTracking(true);
        String res = req.getParameter("taintedValue");
        String unsafe = "abc" + res;
        String safe = "abc" + "foobar";
        assertTrue(Tainter.isTainted(unsafe));
        assertFalse(Tainter.isTainted(safe));
        
    }
    
}
