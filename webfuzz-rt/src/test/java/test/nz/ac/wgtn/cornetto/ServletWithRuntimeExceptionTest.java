package test.nz.ac.wgtn.cornetto;

import nz.ac.wgtn.cornetto.jee.rt.Constants;
import nz.ac.wgtn.cornetto.jee.rt.InvocationTrackerManagerFilter;
import nz.ac.wgtn.cornetto.jee.rt.TrackedInvocationsPickupServlet;
import org.junit.Test;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockFilterConfig;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.junit.Assert.*;

public class ServletWithRuntimeExceptionTest {


    @Test
    public void testRuntimeExceptionTracking () throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        FilterConfig config = new MockFilterConfig();
        FilterChain chain = new MockFilterChain() {
            @Override
            public void doFilter(ServletRequest req, ServletResponse res) throws ServletException, IOException {
                try {
                    new ServletWithRuntimeException().doGet((HttpServletRequest) req, (HttpServletResponse) res);
                } catch (Exception e) {

                }
            }
        };

        InvocationTrackerManagerFilter invocationTrackerManagerFilter = new InvocationTrackerManagerFilter();
        invocationTrackerManagerFilter.init(config);
        invocationTrackerManagerFilter.doFilter(request,response,chain);
        invocationTrackerManagerFilter.destroy();

        String ticket = response.getHeader(Constants.FUZZING_FEEDBACK_TICKET_HEADER);

        assertNotNull(ticket);

        // part 2: pick up recorded invocations
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();
        TrackedInvocationsPickupServlet pickup = new TrackedInvocationsPickupServlet();
        request.setPathInfo(ticket);
        pickup.doGet(request,response);

        assertEquals("application/json",response.getContentType());
        String content = response.getContentAsString();
        System.err.println(content);

    }

}
