#!/bin/bash

## SCRIPT TO RUN FUZZING CAMPAIGNS FOR PYBBS
NUMBER_OF_EXPERIMENTS=3
CAMPAIGN_RUNTIME=480
MAX_500_REPORTED=10000
MAX_XSS_REPORTED=10000
MAX_INJECTIONS_REPORTED=10000
PYBBS_WAR=examples/pybbs/pybbs.jar
echo "building project"
mvn clean package

for ((i = 1; i <= ${NUMBER_OF_EXPERIMENTS}; i++)); do
  echo "iteration ${i}"
  echo "starting server"
  java -javaagent:tools/aspectjweaver-1.9.6.jar -jar ${PYBBS_WAR} >/dev/null 2>&1 &
  server_pid=$!
  echo "server process is ${server_pid}"
  sleep 5m

  #start fuzzing campaign

  # copy dependency to easily build classpath
  mvn dependency:copy-dependencies
  if [ $# -eq 1 ]; then
    HARDIR=$1
    java -Xmx20g -cp target/cornetto.jar:target/dependency/* nz.ac.wgtn.cornetto.jee.FuzzPybbs -outputRootFolder -replayHarDir ${HARDIR} results-pybbs-${i} -campaignLength ${CAMPAIGN_RUNTIME} -max500Reported ${MAX_500_REPORTED} -maxXSSReported ${MAX_XSS_REPORTED} -maxInjectionsReported ${MAX_INJECTIONS_REPORTED} -trials 500000 -requesttimeout 10000 -requestqueuesize 1000 -responsequeuesize 1000 -responseevaluationthreads 2 -requestexecutionthreads 3 -requestgenerationthreads 2
  else
    echo "running without har files"
    java -Xmx20g -cp target/cornetto.jar:target/dependency/* nz.ac.wgtn.cornetto.jee.FuzzPybbs -outputRootFolder results-pybbs-${i} -campaignLength ${CAMPAIGN_RUNTIME} -max500Reported ${MAX_500_REPORTED} -maxXSSReported ${MAX_XSS_REPORTED} -maxInjectionsReported ${MAX_INJECTIONS_REPORTED} -trials 500000 -requesttimeout 10000 -requestqueuesize 1000 -responsequeuesize 1000 -responseevaluationthreads 2 -requestexecutionthreads 3 -requestgenerationthreads 2

  fi
  #zip results, remove folder to save space
  zip -r results/results-pybbs-${i}.zip results-pybbs-${i}
  rm results-pybbs-${i}/*
  rm -r results-pybbs-${i}
  echo "iteration ${i} done"

  #kill server for clean restart
  echo "killing server process ${server_pid}"
  kill -9 ${server_pid}
  sleep 10s
done

echo "done !"
