#!/bin/bash

## SCRIPT TO RUN FUZZING CAMPAIGNS FOR SHOPIZER IN BATCH MODE
NUMBER_OF_EXPERIMENTS=3
CAMPAIGN_RUNTIME=480
HARDIR=""
webapp=$TOMCAT_HOME/webapps/shopizer.fuzz

echo "building preparer"
cd ./war-preparer
mvn clean package
echo "building fuzzer"
cd ..
mvn clean package
workdir=$(pwd)

for ((i = 1; i <= ${NUMBER_OF_EXPERIMENTS}; i++)); do
  echo "iteration ${i}"
  if test -f "$webapp"; then
    rm -r -f "$webapp"
  fi
  cd $TOMCAT_HOME/bin/
  ./startup.sh
  sleep 30s
  cd $workdir
  if [ $# -eq 1 ]; then
    HARDIR=$1
    ant -buildfile run.xml -propertyfile campaigns/shopizer.properties -DcampaignLength=${CAMPAIGN_RUNTIME} -DoutputRootFolder=results-shopizer-${i} -DreplayHarDir=${HARDIR}
  else
    echo "running without har files"
    ant -buildfile run.xml -propertyfile campaigns/shopizer.properties -DcampaignLength=${CAMPAIGN_RUNTIME} -DoutputRootFolder=results-shopizer-${i}
  fi
  #zip results, remove folder to save space
  zip -r results/results-shopizer-${i}.zip results-shopizer-${i}
  rm results-shopizer-${i}/*
  rm -r results-shopizer-${i}
  cd $TOMCAT_HOME/bin/
  ./shutdown.sh
  sleep 30s
  echo "iteration ${i} done"
done

echo "done !"
