# crawljaxproxy

Use of [Crawljax](https://github.com/crawljax/crawljax) with [ZAP](https://github.com/zaproxy/zaproxy). Records crawl requests to HAR file.
You may need to update certificate if it became invalidate in config.xml (line 528)
### Usage
arg[0]= target url, this will only record urls that match the target.

to stop the proxy server, type "quit" in the console to stop and write output to out.har.

proxy server:127.0.0.1  port:8089

```
mvn clean compile
mvn dependency:copy-dependencies
java -cp target/classes:target/dependency/* cornetto.Main http://<host>:8000
```


Output HAR log is written to `out.har`.

**NOTE**: the target host should not be localhost or 127.0.0.1 as that can bypass the proxy. Use the external interface's IP address.
