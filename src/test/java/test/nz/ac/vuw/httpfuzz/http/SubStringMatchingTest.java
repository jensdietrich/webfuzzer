package test.nz.ac.vuw.httpfuzz.http;

import nz.ac.wgtn.cornetto.xss.DefaultXSSDetectionEngine;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * tests for substring matching in xss detection
 * @author Li Sui
 */
public class SubStringMatchingTest {

    private static DefaultXSSDetectionEngine engine;
    @BeforeClass
    public static void beforeClass() throws Exception {
        engine=new DefaultXSSDetectionEngine();
    }

    @AfterClass
    public static void afterClass() throws Exception {
        engine=null;
    }

    @Test
    public void testAlterTheLastChar(){
        String response="SafeTexZ";
        String payload="SafeText";
        Assert.assertTrue(engine.matchTrackedString(response,payload));
    }

    @Test
    public void testRemoveTheLastChar(){
        String response="SafeTex";
        String payload="SafeText";
        Assert.assertTrue(engine.matchTrackedString(response,payload));
    }

    @Test
    public void testWithinURI(){
        String response="http://localhost:8080/benchmark/xss-00/BenchmarkTest00291.html?BenchmarkTest00291=SafeTex";
        String payload="SafeText";
        Assert.assertTrue(engine.matchTrackedString(response,payload));
    }

    @Test
    public void testWithinString(){
        String response="Formatted like: SafeTexZ and b.";
        String payload="SafeText";
        Assert.assertTrue(engine.matchTrackedString(response,payload));
    }

    @Test
    public void testWithinLongString(){
        String response="<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n" +
                "<!-- saved from url=(0090)http://localhost:8080/benchmark/xss-04/BenchmarkTest02324.html?BenchmarkTest02324=SafeText -->\n" +
                "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n" +
                "\n" +
                "<script src=\"./BenchmarkTest02324_files/jquery.min.js\" type=\"text/javascript\"></script>\n" +
                "<script type=\"text/javascript\" src=\"./BenchmarkTest02324_files/js.cookie.js\"></script>\n" +
                "<title>BenchmarkTest02324</title>\n" +
                "</head>\n" +
                "<body data-new-gr-c-s-check-loaded=\"14.1041.0\" data-gr-ext-installed=\"\">\n" +
                "    <form action=\"http://localhost:8080/benchmark/xss-04/BenchmarkTest02324\" method=\"POST\" id=\"FormBenchmarkTest02324\">\n" +
                "        <div>\n" +
                "            <label>Please explain your answer:</label>\n" +
                "        </div>\n" +
                "        <br>\n" +
                "        <div>\n" +
                "            <textarea rows=\"4\" cols=\"50\" id=\"BenchmarkTest02324Area\" name=\"BenchmarkTest02324Area\"></textarea>\n" +
                "        </div>\n" +
                "        <div>\n" +
                "            <label>Any additional note for the reviewer:</label>\n" +
                "        </div>\n" +
                "        <div>\n" +
                "            <input type=\"text\" id=\"answer\" name=\"answer\">\n" +
                "        </div>\n" +
                "        <br>\n" +
                "        <div>\n" +
                "            <label>An AJAX request will be sent with a parameter value 'BenchmarkTest02324' and name:</label> <input type=\"text\" id=\"BenchmarkTest02324\" name=\"BenchmarkTest02324\" value=\"SafeText\" class=\"safe\">\n" +
                "        </div>\n" +
                "        <div>\n" +
                "            <input type=\"button\" id=\"login-btn\" value=\"Login\" onclick=\"submitForm()\">\n" +
                "        </div>\n" +
                "    <input type=\"hidden\" value=\"BenchmarkTest02324\" id=\"SafeText\" name=\"SafeText\" class=\"headerClass\"></form>\n" +
                "    <div id=\"ajax-form-msg1\">\n" +
                "        <pre>            <code class=\"prettyprint\" id=\"code\">Formatted like: SafeTexZ and b.</code>\n" +
                "        </pre>\n" +
                "    </div>\n" +
                "    <script type=\"text/javascript\">\n" +
                "        $('.safe').keypress(function (e) {\n" +
                "            if (e.which == 13) {\n" +
                "                submitForm();\n" +
                "                return false;\n" +
                "            }\n" +
                "        });\n" +
                "\n" +
                "        function submitForm() {\n" +
                "            var text = $(\"#FormBenchmarkTest02324 input[id=BenchmarkTest02324]\")\n" +
                "                    .val();\n" +
                "\n" +
                "            $(\"input.headerClass\").remove();\n" +
                "\n" +
                "            $(\"<input type='hidden' value='BenchmarkTest02324' />\").attr(\"id\",\n" +
                "                    text).attr(\"name\", text).addClass(\"headerClass\").appendTo(\n" +
                "                    \"#FormBenchmarkTest02324\");\n" +
                "\n" +
                "            var formData = $(\"#FormBenchmarkTest02324\").serialize();\n" +
                "            var URL = $(\"#FormBenchmarkTest02324\").attr(\"action\");\n" +
                "\n" +
                "            var xhr = new XMLHttpRequest();\n" +
                "            xhr.open(\"POST\", URL, true);\n" +
                "            xhr.setRequestHeader('Content-Type',\n" +
                "                    'application/x-www-form-urlencoded');\n" +
                "\n" +
                "            xhr.onreadystatechange = function () {\n" +
                "                if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {\n" +
                "                    $(\"#code\").html(xhr.responseText);\n" +
                "                } else {\n" +
                "                    $(\"#code\").text(\n" +
                "                            \"Error \" + xhr.status + \" \" + xhr.statusText\n" +
                "                                    + \" occurred.\");\n" +
                "                }\n" +
                "            }\n" +
                "            xhr.send(formData);\n" +
                "        }\n" +
                "\n" +
                "        function escapeRegExp(str) {\n" +
                "            return str.replace(/([.*+?^=!:${}()|\\[\\]\\/\\\\])/g, \"\\\\$1\");\n" +
                "        }\n" +
                "\n" +
                "        function replaceAll(str, find, replace) {\n" +
                "            return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);\n" +
                "        }\n" +
                "\n" +
                "        String.prototype.decodeEscapeSequence = function () {\n" +
                "            var txt = replaceAll(this, \";\", \"\");\n" +
                "            txt = replaceAll(txt, \"&#\", \"\\\\\");\n" +
                "            return txt.replace(/\\\\x([0-9A-Fa-f]{2})/g, function () {\n" +
                "                return String.fromCharCode(parseInt(arguments[1], 16));\n" +
                "            });\n" +
                "        };\n" +
                "    </script>\n" +
                "\n" +
                "\n" +
                "</body><grammarly-desktop-integration data-grammarly-shadow-root=\"true\"></grammarly-desktop-integration></html>";
        String payload="SafeText";
        Assert.assertTrue(engine.matchTrackedString(response,payload));
    }

    @Test
    public void testNotWithinLongString(){
        String response="<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n" +
                "<!-- saved from url=(0090)http://localhost:8080/benchmark/xss-04/BenchmarkTest02324.html?BenchmarkTest02324=SafeText -->\n" +
                "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n" +
                "\n" +
                "<script src=\"./BenchmarkTest02324_files/jquery.min.js\" type=\"text/javascript\"></script>\n" +
                "<script type=\"text/javascript\" src=\"./BenchmarkTest02324_files/js.cookie.js\"></script>\n" +
                "<title>BenchmarkTest02324</title>\n" +
                "</head>\n" +
                "<body data-new-gr-c-s-check-loaded=\"14.1041.0\" data-gr-ext-installed=\"\">\n" +
                "    <form action=\"http://localhost:8080/benchmark/xss-04/BenchmarkTest02324\" method=\"POST\" id=\"FormBenchmarkTest02324\">\n" +
                "        <div>\n" +
                "            <label>Please explain your answer:</label>\n" +
                "        </div>\n" +
                "        <br>\n" +
                "        <div>\n" +
                "            <textarea rows=\"4\" cols=\"50\" id=\"BenchmarkTest02324Area\" name=\"BenchmarkTest02324Area\"></textarea>\n" +
                "        </div>\n" +
                "        <div>\n" +
                "            <label>Any additional note for the reviewer:</label>\n" +
                "        </div>\n" +
                "        <div>\n" +
                "            <input type=\"text\" id=\"answer\" name=\"answer\">\n" +
                "        </div>\n" +
                "        <br>\n" +
                "        <div>\n" +
                "            <label>An AJAX request will be sent with a parameter value 'BenchmarkTest02324' and name:</label> <input type=\"text\" id=\"BenchmarkTest02324\" name=\"BenchmarkTest02324\" value=\"SafeText\" class=\"safe\">\n" +
                "        </div>\n" +
                "        <div>\n" +
                "            <input type=\"button\" id=\"login-btn\" value=\"Login\" onclick=\"submitForm()\">\n" +
                "        </div>\n" +
                "    <input type=\"hidden\" value=\"BenchmarkTest02324\" id=\"SafeText\" name=\"SafeText\" class=\"headerClass\"></form>\n" +
                "    <div id=\"ajax-form-msg1\">\n" +
                "        <pre>            <code class=\"prettyprint\" id=\"code\">Formatted like: SafeTexZ and b.</code>\n" +
                "        </pre>\n" +
                "    </div>\n" +
                "    <script type=\"text/javascript\">\n" +
                "        $('.safe').keypress(function (e) {\n" +
                "            if (e.which == 13) {\n" +
                "                submitForm();\n" +
                "                return false;\n" +
                "            }\n" +
                "        });\n" +
                "\n" +
                "        function submitForm() {\n" +
                "            var text = $(\"#FormBenchmarkTest02324 input[id=BenchmarkTest02324]\")\n" +
                "                    .val();\n" +
                "\n" +
                "            $(\"input.headerClass\").remove();\n" +
                "\n" +
                "            $(\"<input type='hidden' value='BenchmarkTest02324' />\").attr(\"id\",\n" +
                "                    text).attr(\"name\", text).addClass(\"headerClass\").appendTo(\n" +
                "                    \"#FormBenchmarkTest02324\");\n" +
                "\n" +
                "            var formData = $(\"#FormBenchmarkTest02324\").serialize();\n" +
                "            var URL = $(\"#FormBenchmarkTest02324\").attr(\"action\");\n" +
                "\n" +
                "            var xhr = new XMLHttpRequest();\n" +
                "            xhr.open(\"POST\", URL, true);\n" +
                "            xhr.setRequestHeader('Content-Type',\n" +
                "                    'application/x-www-form-urlencoded');\n" +
                "\n" +
                "            xhr.onreadystatechange = function () {\n" +
                "                if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {\n" +
                "                    $(\"#code\").html(xhr.responseText);\n" +
                "                } else {\n" +
                "                    $(\"#code\").text(\n" +
                "                            \"Error \" + xhr.status + \" \" + xhr.statusText\n" +
                "                                    + \" occurred.\");\n" +
                "                }\n" +
                "            }\n" +
                "            xhr.send(formData);\n" +
                "        }\n" +
                "\n" +
                "        function escapeRegExp(str) {\n" +
                "            return str.replace(/([.*+?^=!:${}()|\\[\\]\\/\\\\])/g, \"\\\\$1\");\n" +
                "        }\n" +
                "\n" +
                "        function replaceAll(str, find, replace) {\n" +
                "            return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);\n" +
                "        }\n" +
                "\n" +
                "        String.prototype.decodeEscapeSequence = function () {\n" +
                "            var txt = replaceAll(this, \";\", \"\");\n" +
                "            txt = replaceAll(txt, \"&#\", \"\\\\\");\n" +
                "            return txt.replace(/\\\\x([0-9A-Fa-f]{2})/g, function () {\n" +
                "                return String.fromCharCode(parseInt(arguments[1], 16));\n" +
                "            });\n" +
                "        };\n" +
                "    </script>\n" +
                "\n" +
                "\n" +
                "</body><grammarly-desktop-integration data-grammarly-shadow-root=\"true\"></grammarly-desktop-integration></html>";
        String payload="thisisTest123";
        Assert.assertFalse(engine.matchTrackedString(response,payload));
    }
}
