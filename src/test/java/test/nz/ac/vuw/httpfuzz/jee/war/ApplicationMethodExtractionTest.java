package test.nz.ac.vuw.httpfuzz.jee.war;

import com.google.common.base.Preconditions;
import nz.ac.wgtn.cornetto.StaticModel;
import nz.ac.wgtn.cornetto.jee.war.ApplicationMethodExtractor;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.io.File;
import java.util.EnumSet;
import java.util.Set;

import static org.junit.Assert.*;

public class ApplicationMethodExtractionTest {
    private File war = null;
    private static EnumSet<StaticModel.Scope> SCOPES = EnumSet.of(StaticModel.Scope.APPLICATION);

    @Before
    public void setup() throws Exception {
        war = new File("src/test/resources/war-webxml1/target/war-webxml1-1.1.0.war");
        Preconditions.checkState(war.exists(),"war " + war.getAbsolutePath() + " does not exist, check whether test project in src/test/resources/war-webxml1/ has been built with \"mvn package\"");
    }

    @After
    public void tearDown () {
        war = null;
    }

    @Test
    public void test () throws Exception {
        Set<String> methods = ApplicationMethodExtractor.extractApplicationMethods(war,cl -> cl.startsWith("example1"));
        assertEquals(8,methods.size());
        assertTrue(methods.contains("example1.MainServlet::foo1(Ljava/lang/String;)V"));
        assertTrue(methods.contains("example1.FooServlet::<init>()V"));
        assertTrue(methods.contains("example1.MainServlet::foo3()V"));
        assertTrue(methods.contains("example1.MainServlet::<init>()V"));
        assertTrue(methods.contains("example1.MainServlet::foo2()V"));
        assertTrue(methods.contains("example1.MainServlet::doGet(Ljavax/servlet/http/HttpServletRequest;Ljavax/servlet/http/HttpServletResponse;)V"));
        assertTrue(methods.contains("example1.FooServlet::doGet(Ljavax/servlet/http/HttpServletRequest;Ljavax/servlet/http/HttpServletResponse;)V"));
        assertTrue(methods.contains("example1.MainServlet::foo4()V"));
    }

}
