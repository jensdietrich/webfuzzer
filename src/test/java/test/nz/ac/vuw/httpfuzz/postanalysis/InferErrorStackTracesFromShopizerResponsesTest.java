package test.nz.ac.vuw.httpfuzz.postanalysis;

import com.fasterxml.jackson.databind.ObjectMapper;
import nz.ac.wgtn.cornetto.output.ServerError;
import nz.ac.wgtn.cornetto.output.StackTrace;
import nz.ac.wgtn.cornetto.postanalysis.InferErrorStackTracesFromShopizerResponses;
import org.junit.Test;
import java.io.File;
import static org.junit.Assert.*;

/**
 * Tests for error stacktraces found in shopizer
 * The parser can be found in nz.ac.wgtn.cornetto.postanalysis.InferErrorStackTracesFromShopizerResponses
 * @author Li Sui
 */
public class InferErrorStackTracesFromShopizerResponsesTest {

    private StackTrace readStacktrace(String resourceName) throws Exception {
        File input = new File(getClass().getResource(resourceName).getFile());
        ObjectMapper objectMapper = new ObjectMapper();
        ServerError error = objectMapper.readValue(input,ServerError.class);
        return new InferErrorStackTracesFromShopizerResponses().extractStackTrace(error);
    }
    @Test
    public void test1() throws Exception {
        StackTrace stacktrace = readStacktrace("/postanalysis/shopizer/server-error-1.json");
        assertNotNull(stacktrace);
        assertEquals("java.lang.IllegalStateException",stacktrace.getExceptionType());
        assertEquals("No primary or default constructor found for class java.lang.Integer",stacktrace.getExceptionMessage());
        assertEquals("org.springframework.web.method.annotation.ModelAttributeMethodProcessor.createAttribute(ModelAttributeMethodProcessor.java:219)",stacktrace.getEntries().get(0));
        assertEquals("org.springframework.web.servlet.mvc.method.annotation.ServletModelAttributeMethodProcessor.createAttribute(ServletModelAttributeMethodProcessor.java:85)",stacktrace.getEntries().get(1));
    }
}
