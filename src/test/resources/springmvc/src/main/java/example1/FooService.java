package example1;

import org.springframework.web.bind.annotation.*;

// examples from https://www.baeldung.com/spring-requestmapping
public class FooService  {
    @RequestMapping(value = "/ex/foos", method = RequestMethod.POST)
    @ResponseBody
    public String postFoos() {
        return "Post some Foos";
    }

    @RequestMapping(value = "/ex/foos/{id}", method = RequestMethod.GET)
    @ResponseBody
    public String getFoosBySimplePathWithPathVariable(@PathVariable("id") long id) {
        return "Get a specific Foo with id=" + id;
    }

    @GetMapping(value = "/ex/foos/{fooid}/bar/{barid}")
    @ResponseBody
    public String getFoosBySimplePathWithPathVariables (@PathVariable long fooid, @PathVariable long barid) {
        return "Get a specific Bar with id=" + barid + " from a Foo with id=" + fooid;
    }

}