<?xml version="1.0"?>
<callsites>
    <type>DATABIND</type>
    <format>JSON</format>
    <generated>false</generated>
    <name>jackson-bind</name>
    <patterns>
        <pattern>
            <classname>com.fasterxml.jackson.databind.ObjectMapper</classname>
            <methodname>readValue</methodname>
        </pattern>
    </patterns>
    <namespace>com.fasterxml.jackson</namespace>
</callsites>

