<?xml version="1.0"?>
<callsites>
    <type>PUSH</type>
    <format>XML</format>
    <generated>false</generated>
    <name>aalto-push</name>
    <patterns>
        <pattern>
            <classname>com.fasterxml.aalto.sax.SAXParserImpl</classname>
        </pattern>
        <pattern>
            <classname>com.fasterxml.aalto.sax.SAXParserFactoryImpl</classname>
        </pattern>
    </patterns>
    <namespace>com.fasterxml.aalto</namespace>
</callsites>

