<?xml version="1.0"?>
<callsites>
    <type>PULL</type>
    <format>JSON</format>
    <generated>false</generated>
    <name>jsonp-pull</name>
    <patterns>
        <pattern>
            <classname>javax.json.Json</classname>
            <methodname>createParser</methodname>
        </pattern>
    </patterns>
    <namespace>javax.json</namespace>
</callsites>

