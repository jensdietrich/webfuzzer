<?xml version="1.0"?>
<callsites>
    <type>PATHQUERY</type>
    <format>JSON</format>
    <generated>false</generated>
    <name>jsonp-path</name>
    <patterns>
        <pattern>
            <classname>javax.json.Json</classname>
            <methodname>createPointer</methodname>
        </pattern>
    </patterns>
    <namespace>javax.json</namespace>
</callsites>

