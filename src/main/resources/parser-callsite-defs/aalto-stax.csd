<?xml version="1.0"?>
<callsites>
    <type>PULL</type>
    <format>XML</format>
    <generated>false</generated>
    <name>aalto-pull</name>
    <patterns>
        <pattern>
            <classname>com.fasterxml.aalto.AsyncXMLStreamReader</classname>
        </pattern>
        <pattern>
            <classname>com.fasterxml.aalto.stax.StreamReaderImpl</classname>
        </pattern>
        <pattern>
            <classname>com.fasterxml.aalto.stax.InputFactoryImpl</classname>
        </pattern>
    </patterns>
    <namespace>com.fasterxml.aalto</namespace>
</callsites>

