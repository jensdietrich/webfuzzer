<?xml version="1.0"?>
<callsites>
    <type>DOM</type>
    <format>XML</format>
    <generated>false</generated>
    <name>jdom-dom</name>
    <patterns>
        <!-- different lower-level parsing strategies can be used to build the jdom representation -->
        <pattern>
            <classname>org.jdom.input.SAXBuilder</classname>
            <methodname>build</methodname>
        </pattern>
        <pattern>
            <classname>org.jdom.input.DOMBuilder</classname>
            <methodname>build</methodname>
        </pattern>
    </patterns>
    <namespace>org.jdom</namespace>
</callsites>

