<?xml version="1.0"?>
<callsites>
    <type>PULL</type>
    <format>JSON</format>
    <generated>false</generated>
    <name>gson-pull</name>
    <patterns>
        <pattern>
            <classname>com.google.gson.stream.JsonReader</classname>
            <methodname>&lt;init&gt;</methodname>
        </pattern>
    </patterns>
    <namespace>com.google.gson</namespace>
</callsites>

