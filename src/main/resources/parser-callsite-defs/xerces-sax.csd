<?xml version="1.0"?>
<callsites>
    <type>PUSH</type>
    <format>XML</format>
    <generated>false</generated>
    <name>xerces-push</name>
    <patterns>
        <pattern>
            <classname>org.apache.xerces.parsers.SAXParser</classname>
        </pattern>
        <pattern>
            <classname>org.apache.xerces.jaxp.SAXParserImpl</classname>
        </pattern>
        <pattern>
            <classname>org.apache.xerces.jaxp.SAXParserFactoryImpl</classname>
        </pattern>
    </patterns>
    <namespace>org.apache.xerces</namespace>
</callsites>

