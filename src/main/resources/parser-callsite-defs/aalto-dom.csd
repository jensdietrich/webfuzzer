<?xml version="1.0"?>
<callsites>
    <type>DOM</type>
    <format>XML</format>
    <generated>false</generated>
    <name>aalto-dom</name>
    <patterns>
        <!-- this one is strange: it provide a stax API to access a DOM, but an intermediate DOM is still being built -->
        <pattern>
            <classname>com.fasterxml.aalto.dom.DOMReaderImpl</classname>
        </pattern>
    </patterns>
    <namespace>com.fasterxml.aalto</namespace>
</callsites>

