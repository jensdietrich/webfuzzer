<?xml version="1.0"?>
<callsites>
    <type>DATABIND</type>
    <format>JSON</format>
    <generated>false</generated>
    <name>fastjson-bind</name>
    <patterns>
        <pattern>
            <classname>com.alibaba.fastjson.JSON</classname>
            <methodname>parseObject</methodname>
            <descriptor>(.*)Ljava/lang/Object;</descriptor>
        </pattern>
        <pattern>
            <classname>com.alibaba.fastjson.JSON</classname>
            <methodname>parseArray</methodname>
            <descriptor>.*Ljava/util/List;</descriptor>
        </pattern>
    </patterns>
    <namespace>com.alibaba.fastjson</namespace>
</callsites>

