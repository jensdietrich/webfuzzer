<?xml version="1.0"?>
<callsites>
    <type>DOM</type>
    <format>XML</format>
    <generated>false</generated>
    <name>dom4j-dom</name>
    <patterns>
        <!-- different lower-level parsing strategies can be used to build the dom4j representation -->
        <pattern>
            <classname>org.dom4j.io.SAXReader</classname>
            <methodname>read</methodname>
        </pattern>
        <pattern>
            <classname>org.dom4j.io.DOMReader</classname>
            <methodname>read</methodname>
        </pattern>
        <pattern>
            <classname>org.dom4j.io.STAXEventReader</classname>
            <methodname>readDocument</methodname>
        </pattern>
        <pattern>
            <classname>org.dom4j.io.XPPReader</classname>
            <methodname>read</methodname>
        </pattern>
        <pattern>
            <classname>org.dom4j.io.XPP3Reader</classname>
            <methodname>read</methodname>
        </pattern>
    </patterns>
    <namespace>org.dom4j</namespace>
</callsites>

