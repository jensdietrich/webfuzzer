<?xml version="1.0"?>
<callsites>
    <type>PUSH</type>
    <format>JSON</format>
    <generated>false</generated>
    <name>minimaljson-push</name>
    <patterns>
        <pattern>
            <classname>com.eclipsesource.json.JsonParser</classname>
            <methodname>parse</methodname>
        </pattern>
    </patterns>
    <namespace>com.eclipsesource.json</namespace>
</callsites>

