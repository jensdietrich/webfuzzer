<?xml version="1.0"?>
<callsites>
    <type>DOM</type>
    <format>XML</format>
    <generated>false</generated>
    <name>crimson-dom</name>
    <patterns>
        <pattern>
            <classname>org.apache.crimson.tree.XmlDocumentBuilder</classname>
        </pattern>
        <pattern>
            <classname>org.apache.crimson.jaxp.DocumentBuilderImpl</classname>
        </pattern>
        <pattern>
            <classname>org.apache.crimson.jaxp.DocumentBuilderFactoryImpl</classname>
        </pattern>
    </patterns>
    <namespace>org.apache.crimson</namespace>
</callsites>

