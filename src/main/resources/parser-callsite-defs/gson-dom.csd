<?xml version="1.0"?>
<callsites>
    <type>DOM</type>
    <format>JSON</format>
    <generated>false</generated>
    <name>gson-dom</name>
    <patterns>
        <pattern>
            <classname>com.google.gson.JsonParser</classname>
            <methodname>parse</methodname>
        </pattern>
    </patterns>
    <namespace>com.google.gson</namespace>
</callsites>

