package nz.ac.wgtn.cornetto.http;

import nz.ac.wgtn.cornetto.Context;
import nz.ac.wgtn.cornetto.Generator;
import nz.ac.wgtn.cornetto.commons.SourceOfRandomness;

public class MethodSpecGenerator implements Generator<MethodSpec> {

    @Override
    public MethodSpec apply(SourceOfRandomness sourceOfRandomness, Context context) {
        return sourceOfRandomness.choose(context.getProfile().getUsedMethods());
    }
}
