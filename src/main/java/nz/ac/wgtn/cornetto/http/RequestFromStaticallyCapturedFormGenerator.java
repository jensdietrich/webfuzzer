package nz.ac.wgtn.cornetto.http;

import com.google.common.base.Preconditions;
import nz.ac.wgtn.cornetto.Context;
import nz.ac.wgtn.cornetto.SpecSource;
import nz.ac.wgtn.cornetto.commons.SourceOfRandomness;
import nz.ac.wgtn.cornetto.html.Form;

import java.util.Collection;
import java.util.EnumSet;

/**
 * Generate a request from a form captured from parsed pages during pre-analysis.
 * @author jens dietrich
 */
public class RequestFromStaticallyCapturedFormGenerator extends AbstractRequestFromCapturedFormGenerator {

    // this is used to point to the origin of the form
    protected EnumSet<SpecSource> getProvenance() {
        return EnumSet.of(SpecSource.STATIC_ANALYSIS);
    }

    protected Form getForm(SourceOfRandomness sourceOfRandomness, Context context) {
        Collection<Form> forms = context.getStaticModel().getForms();
        // callers must ensure this
        Preconditions.checkState(!forms.isEmpty(),"No forms have been recorded");
        Form form = sourceOfRandomness.choose(forms);
        return form;
    }

}
