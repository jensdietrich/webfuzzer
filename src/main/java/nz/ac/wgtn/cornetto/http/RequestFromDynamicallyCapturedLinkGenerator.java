package nz.ac.wgtn.cornetto.http;

import nz.ac.wgtn.cornetto.Context;
import nz.ac.wgtn.cornetto.SpecSource;
import nz.ac.wgtn.cornetto.commons.SourceOfRandomness;

import java.net.URL;
import java.util.EnumSet;

/**
 * Generate a request from a link captured from a page returned by the server.
 * @author jens dietrich
 */
public class RequestFromDynamicallyCapturedLinkGenerator extends AbstractRequestFromCapturedLinkGenerator{

    @Override
    protected EnumSet<SpecSource> getProvenance() {
        return EnumSet.of(SpecSource.DYNAMIC_ANALYSIS);
    }

    @Override
    protected URL getLink(SourceOfRandomness sourceOfRandomness, Context context) {
        return sourceOfRandomness.choose(context.getDynamicModel().getLinks());
    }
}
