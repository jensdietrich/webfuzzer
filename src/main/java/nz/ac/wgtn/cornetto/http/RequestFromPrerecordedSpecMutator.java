package nz.ac.wgtn.cornetto.http;

import com.google.common.base.Preconditions;
import nz.ac.wgtn.cornetto.Context;
import nz.ac.wgtn.cornetto.Generator;
import nz.ac.wgtn.cornetto.SpecSource;
import nz.ac.wgtn.cornetto.commons.SourceOfRandomness;

import java.util.Collection;
import java.util.EnumSet;


public class RequestFromPrerecordedSpecMutator implements Generator<RequestSpec> {

    private ParametersSpecMutator parametersMutator = new ParametersSpecMutator();

    // TODO: headers, methods

    public RequestFromPrerecordedSpecMutator() {
        super();
    }

    @Override
    public RequestSpec apply(SourceOfRandomness sor, Context context) {

        Collection<RequestSpec> specs = context.getDynamicModel().getRequestSpecs();
        Collection<RequestSpec> recordedSpecs = context.getDynamicModel().getPrerecordedRequests();
        Preconditions.checkState(recordedSpecs.size()>0);

        RequestSpec original = sor.choose(recordedSpecs);
        RequestSpec requestSpec = new RequestSpec();

        //requestSpec.setParametersSpec(this.parametersMutator.apply(sor,context,original.getParametersSpec()));
        requestSpec.setUriSpec(original.getUriSpec());

        // TODO: mutate those as well
        HeadersSpec originalHeadersSpec = original.getHeadersSpec();
        HeadersSpec headersSpec = new HeadersSpec();
        assert !originalHeadersSpec.getHeaders().isEmpty();

        HeaderSpec toBeTainted = sor.choose(originalHeadersSpec.getHeaders());
        for (HeaderSpec header:originalHeadersSpec.getHeaders()) {
            if (header==toBeTainted) {
                boolean taintKey = sor.nextBoolean();
                ValueSpec taint = TaintedStringValueGenerator.DEFAULT.apply(sor,context);
                ValueSpec key = taintKey ? taint : header.getKey();
                ValueSpec value = taintKey ? header.getValue() : taint;
                HeaderSpec taintedHeader = new HeaderSpec(key,value,header.getSources());
                headersSpec.add(taintedHeader);
            }
            else {
                headersSpec.add(header);
            }
        }
        ParametersSpec paramsSpec, originalParamsSpec;
        paramsSpec = new ParametersSpec();
        originalParamsSpec = original.getParametersSpec();
        assert !originalParamsSpec.getParams().isEmpty();

        ParameterSpec toBeTaintedParams = sor.choose(originalParamsSpec.getParams());
        for (ParameterSpec param:originalParamsSpec.getParams()) {
            if (param==toBeTaintedParams) {
                boolean taintKey = sor.nextBoolean();
                ValueSpec taint = TaintedStringValueGenerator.DEFAULT.apply(sor,context);
                ValueSpec key = taintKey ? taint : param.getKey();
                ValueSpec value = taintKey ? param.getValue() : taint;
                ParameterSpec taintedParam = new ParameterSpec(key,value,param.getSources());
                paramsSpec.add(taintedParam);
            }
            else {
                paramsSpec.add(param);
            }
        }
        requestSpec.setHeadersSpec(headersSpec);
        requestSpec.setMethodSpec(original.getMethodSpec());
        requestSpec.setParametersSpec(paramsSpec);
        EnumSet<SpecSource> provenance = original.getSources().clone();
        provenance.add(SpecSource.MUTATED);
        requestSpec.setProvenance(provenance);

        // copy selected meta data
        requestSpec.addParent(original);

        return requestSpec;
    }
}
