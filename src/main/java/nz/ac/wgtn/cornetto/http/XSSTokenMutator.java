package nz.ac.wgtn.cornetto.http;

import nz.ac.wgtn.cornetto.Context;
import nz.ac.wgtn.cornetto.DynamicModel;
import nz.ac.wgtn.cornetto.Generator;
import nz.ac.wgtn.cornetto.SpecSource;
import nz.ac.wgtn.cornetto.commons.SourceOfRandomness;
import nz.ac.wgtn.cornetto.xss.XSSCandidate;

import java.util.*;

/**
 * Generated new requests by mutating promising requests.
 * @author jens dietrich
 */
public class XSSTokenMutator implements Generator<RequestSpec> {

    @Override
    public synchronized RequestSpec apply(SourceOfRandomness sourceOfRandomness, Context context) {

        DynamicModel dynamicModel = context.getDynamicModel();
        Collection<XSSCandidate> xssCandidates = dynamicModel.getXSSCandidates();
        assert !xssCandidates.isEmpty(); // guard needed at callsite
        XSSCandidate xssCandidate = sourceOfRandomness.choose(xssCandidates);

        RequestSpec originalSpec = xssCandidate.getRequestSpec();
        String originalXSSToken = xssCandidate.getXSSToken();
        assert xssCandidate.hasMoreReplacementsTokens();
        String replacementXSSToken = xssCandidate.getNextReplacementToken();
        RequestSpec mutatedRequestSpec = mutate(originalSpec, originalXSSToken, replacementXSSToken);


        // cleanup
        if (!xssCandidate.hasMoreReplacementsTokens()) {
            dynamicModel.unregisterXSSCandidate(xssCandidate);
        }

        return mutatedRequestSpec;
    }

    // public for unit testing
    public static RequestSpec mutate(RequestSpec originalRequest,String originalXSSToken, String replacementXSSToken) {
        RequestSpec request = new RequestSpec();
        request.setMethodSpec(originalRequest.getMethodSpec());

        // URI -- originalXSSToken could be path element
        URISpec uriSpec = new URISpec();
        uriSpec.setScheme(originalRequest.getUriSpec().getScheme());
        uriSpec.setHost(originalRequest.getUriSpec().getHost());
        uriSpec.setPort(originalRequest.getUriSpec().getPort());

        List<ValueSpec<String>> originalPath = originalRequest.getUriSpec().getPathSpec();
        List<ValueSpec<String>> path = new ArrayList<>();
        for (ValueSpec<String> originalPathElement:originalPath) {
            if (originalPathElement.getValue().equals(originalXSSToken)) {
                ValueSpec<String> pathElement = createMutatedValue(replacementXSSToken);
                path.add(pathElement);
            }
            else {
                path.add(originalPathElement);
            }
        }
        uriSpec.addAllToPath(path);
        request.setUriSpec(uriSpec);

        // headers
        List<HeaderSpec> originalHeaders = originalRequest.getHeadersSpec().getHeaders();
        HeadersSpec headers = new HeadersSpec();
        for (HeaderSpec originalHeader:originalHeaders) {
            boolean thisHeaderMutated = false;
            ValueSpec newKey = null;
            if (originalHeader.getKey().getValue().equals(originalXSSToken)) {
                newKey = createMutatedValue(replacementXSSToken);
                thisHeaderMutated = true;
            }
            else {
                newKey = originalHeader.getKey();
            }
            ValueSpec newValue = null;
            if (originalHeader.getValue().getValue().equals(originalXSSToken)) {
                newValue = createMutatedValue(replacementXSSToken);
                thisHeaderMutated = true;
            }
            else {
                newValue = originalHeader.getValue();
            }
            HeaderSpec header = new HeaderSpec(newKey,newValue,getProvenance(originalRequest.getHeadersSpec().getSources(),thisHeaderMutated));
            headers.add(header);
            for (SpecSource ss:header.getSources()) {
                headers.add(ss);
            }
        }
        request.setHeadersSpec(headers);

        // parameters
        Set<ParameterSpec> originalParameters = originalRequest.getParametersSpec().getParams();
        ParametersSpec parameters = new ParametersSpec();
        for (ParameterSpec originalParameter:originalParameters) {
            boolean thisParameterMutated = false;
            ValueSpec newKey = null;
            if (originalParameter.getKey().getValue().equals(originalXSSToken)) {
                newKey = createMutatedValue(replacementXSSToken);
                thisParameterMutated = true;
            }
            else {
                newKey = originalParameter.getKey();
            }
            ValueSpec newValue = null;
            if (originalParameter.getValue().getValue().equals(originalXSSToken)) {
                newValue = createMutatedValue(replacementXSSToken);
                thisParameterMutated = true;
            }
            else {
                newValue = originalParameter.getValue();
            }
            ParameterSpec parameter = new ParameterSpec(newKey,newValue,getProvenance(originalRequest.getHeadersSpec().getSources(),thisParameterMutated));
            parameters.add(parameter);
            for (SpecSource ss:parameter.getSources()) {
                headers.add(ss);
            }
        }
        request.setParametersSpec(parameters);

        // provenance
        EnumSet<SpecSource> provenance = originalRequest.getSources().clone();
        provenance.add(SpecSource.MUTATED);
        request.setProvenance(provenance);
        request.addParent(originalRequest);

        return request;
    }

    private static ValueSpec<String> createMutatedValue(String replacementXSSToken) {
        return new ValueSpec<>(replacementXSSToken,EnumSet.of(SpecSource.TAINTED,SpecSource.MUTATED));
    }

    private static EnumSet<SpecSource> getProvenance(EnumSet<SpecSource> sources, boolean mutated) {
        EnumSet<SpecSource> newProvenance = EnumSet.noneOf(SpecSource.class);
        newProvenance.addAll(sources);
        if (mutated) {
            newProvenance.add(SpecSource.MUTATED);
        }
        return newProvenance;
    }
}