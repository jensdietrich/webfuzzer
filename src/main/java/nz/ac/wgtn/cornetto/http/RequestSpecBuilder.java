package nz.ac.wgtn.cornetto.http;

import com.google.common.base.Preconditions;
import nz.ac.wgtn.cornetto.SpecSource;

import java.util.EnumSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Utility to build request specs using the builder pattern.
 * Particulary useful for testing.
 * @author jens dietrich
 */
public class RequestSpecBuilder {

    private RequestSpec request = new RequestSpec();
    private EnumSet<SpecSource> provenance = EnumSet.noneOf(SpecSource.class);

    public RequestSpecBuilder() {
        this.request.setProvenance(provenance);
        this.request.setUriSpec(new URISpec());
        this.request.setParametersSpec(new ParametersSpec());
        this.request.setHeadersSpec(new HeadersSpec());
        this.request.setMethodSpec(MethodSpec.GET);

        // some sensible defaults
        this.request.getUriSpec().setScheme("http");
        this.request.getUriSpec().setPort(80);
        this.request.getUriSpec().setHost("localhost");
    }

    public RequestSpecBuilder provenance (EnumSet<SpecSource> provenance) {
        this.provenance = provenance;
        return this;
    }

    public RequestSpecBuilder port (int port) {
        this.request.getUriSpec().setPort(port);
        return this;
    }

    public RequestSpecBuilder scheme (String scheme) {
        this.request.getUriSpec().setScheme(scheme);
        return this;
    }

    public RequestSpecBuilder host (String host) {
        this.request.getUriSpec().setHost(host);
        return this;
    }

    public RequestSpecBuilder method(MethodSpec method) {
        this.request.setMethodSpec(method);
        return this;
    }

    public RequestSpecBuilder path(String... pathElements) {
        List<ValueSpec<String>> p = Stream.of(pathElements)
            .map(s -> new ValueSpec<>(s,provenance))
            .collect(Collectors.toList());
        this.request.getUriSpec().addAllToPath(p);
        return this;
    }

    public RequestSpecBuilder header(String key,String value) {
        this.request.getHeadersSpec().add(new HeaderSpec(new ValueSpec(key,provenance),new ValueSpec(value,provenance),provenance));
        return this;
    }

    public RequestSpecBuilder parameter(String key,String value) {
        this.request.getParametersSpec().add(new ParameterSpec(new ValueSpec(key,provenance),new ValueSpec(value,provenance),provenance));
        return this;
    }

    public RequestSpec build() {
        Preconditions.checkState(this.request.getMethodSpec()!=null,"Method must be set");
        return this.request;
    }

}
