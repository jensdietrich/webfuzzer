package nz.ac.wgtn.cornetto.http;

import nz.ac.wgtn.cornetto.Context;
import nz.ac.wgtn.cornetto.Generator;
import nz.ac.wgtn.cornetto.commons.SourceOfRandomness;
import nz.ac.wgtn.cornetto.html.Form;

import java.util.Collection;

/**
 * Generates requests from forms captured in the static pre-analysis, or from feedback.
 * @author jens dietrich
 */
public class RequestFromCapturedFormGenerator implements Generator<RequestSpec> {

    private RequestFromDynamicallyCapturedFormGenerator generateFromDynamicallyCapturedForms = new RequestFromDynamicallyCapturedFormGenerator();
    private RequestFromStaticallyCapturedFormGenerator generateFromStaticallyCapturedForms = new RequestFromStaticallyCapturedFormGenerator();

    @Override
    public RequestSpec apply(SourceOfRandomness sourceOfRandomness, Context context) {
        Collection<Form> sforms = context.getStaticModel().getForms();
        Collection<Form> dforms = context.getDynamicModel().getForms();
        boolean selection = sourceOfRandomness.trueBasedOnSizeRatio(sforms,dforms);
        RequestSpec requestSpec =  selection ? generateFromStaticallyCapturedForms.apply(sourceOfRandomness,context) : generateFromDynamicallyCapturedForms.apply(sourceOfRandomness,context);
        return requestSpec;
    }
}
