package nz.ac.wgtn.cornetto.util;

import nz.ac.wgtn.cornetto.Profile;
import nz.ac.wgtn.cornetto.jee.Loggers;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Utility to resolve links.
 * @author jens dietrich
 */

public class LinkResolver {

    public static URI resolveRelativeURL(Profile profile, String relPath) throws URISyntaxException {

        Loggers.FUZZER.debug("converting form action to URL: " + relPath);

        String url = null;

        if (relPath.startsWith("http:") || relPath.startsWith("https:")) {
            // establish whether this is "internal" or "external" , skip if external as this is out of scope for fuzzing
            if (relPath.startsWith(profile.getServerURL())) {
                url = relPath;
            }
            else {
                // check for localhost
                if (profile.getHost().equals("localhost")) {
                    if (profile.getProtocol() == Profile.Protocol.http && relPath.startsWith("http://127.0.0.1")) {
                        url =  relPath;
                    }
                    else if (profile.getProtocol() == Profile.Protocol.https && relPath.startsWith("https://127.0.0.1")) {
                        url =  relPath;
                    }
                }
                url =  null;
            }
        }
        else if (relPath.startsWith("~")) {
            Loggers.FUZZER.warn("translation of these form actions not yet implemented: " + relPath);
            return resolveRelativeURL(profile,relPath.substring(1));
        }
        else if (relPath.startsWith("/")) {
            url =  profile.getServerAndPortURL() + relPath ;
        }
        else {
            url =  profile.getWebappRootURL() + "/" + relPath ;
        }
        return url==null?null:new URI(url);

    }
}
