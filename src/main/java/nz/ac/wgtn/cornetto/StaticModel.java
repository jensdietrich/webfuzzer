package nz.ac.wgtn.cornetto;

import nz.ac.wgtn.cornetto.html.Form;
import nz.ac.wgtn.cornetto.http.MethodSpec;

import java.net.URL;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Set;

/**
 * The model exposes information from static analysis that can be used to direct fuzzing.
 * @author jens dietrich
 */
public interface StaticModel {

   // levels can be used as qualifiers when extracting constants
   // this can be used to model priorities and context
   enum Scope {
       APPLICATION, LIBRARIES, SYSTEM
   }

   /**
    * Implementation that can be used if no real static model is used.
    */
   public static StaticModel NULL = new StaticModel() {
      @Override
      public Set<String> getStringLiterals(EnumSet<Scope> scopes) {
         return Collections.emptySet();
      }

      @Override
      public Set<String> getEntryPointsURLPatterns() {
         return Collections.emptySet();
      }

      @Override
      public Set<Integer> getIntLiterals(EnumSet<Scope> scopes) {
         return Collections.emptySet();
      }

      @Override
      public Set<String> getRequestParameterNames() {
         return Collections.emptySet();
      }

      @Override
      public Set<String> getHeaderNames() {
         return Collections.emptySet();
      }

      @Override
      public Set<EntryPoint> getEntryPoints() {
         return Collections.emptySet();
      }

      @Override
      public Set<String> getReflectiveNames(EnumSet<Scope> scopes) {
         return Collections.emptySet();
      }

      @Override
      public Set<MethodSpec> getMethods() {
         return Collections.emptySet();
      }

      @Override
      public Set<Form> getForms() {
         return Collections.emptySet();
      }

      @Override
      public Set<URL> getLinks() {
         return Collections.emptySet();
      }

      @Override
      public Set<String> getApplicationMethods() {
         return Collections.emptySet();
      }

      @Override
      public boolean isApplicationClass(String className) {
         return false;
      }
   };

   /**
    * Get string literals.
    * @param scopes - the scopes for which to extract literals
    * @throws UnsupportedOperationException -- this is an optional method
    * @return
    */
   Set<String> getStringLiterals(EnumSet<Scope> scopes);

   /**
    * Get names of classes and names that can be used for reflection.
    * @param scopes - the scopes for which to extract literals
    * @throws UnsupportedOperationException -- this is an optional method
    * @return
    */
   Set<String> getReflectiveNames(EnumSet<Scope> scopes);

   /**
    * Get int literals.
    * @param scopes - the scopes for which to extract literals
    * @throws UnsupportedOperationException -- this is an optional method
    * @return
    */
   Set<Integer> getIntLiterals(EnumSet<Scope> scopes);

   /**
    * Get (possible) request parameter names.
    * @throws UnsupportedOperationException -- this is an optional method
    * @return
    */
   Set<String> getRequestParameterNames() ;

   /**
    * Get (possible) header names.
    * @throws UnsupportedOperationException -- this is an optional method
    * @return
    */
   Set<String> getHeaderNames() ;

   /**
    * Get entry points (URLs).
    * @throws UnsupportedOperationException -- this is an optional method
    * @return
    */
   Set<EntryPoint> getEntryPoints() ;

   /**
    * Get the URL patterns extracted from the entry points.
    * @return
    */
   Set<String> getEntryPointsURLPatterns() ;

   /**
    * Get supported http method specs.
    * @throws UnsupportedOperationException -- this is an optional method
    * @return
    */
   Set<MethodSpec> getMethods();

   /**
    * Get forms from static html pages, templates etc.
    * @return
    */
   Set<Form> getForms();


   /**
    * Get links from static html pages, templates etc.
    * @return
    */
   Set<URL> getLinks();

   /**
    * Get the application (Java) methods found in the application. Used to calculate coverage.
    * String using the following format:
    *  <classname>::<methodname><descriptor>, with types represented as in source code (i.e., '.', not '/', no leading L for reference types, `int` instead of `I1 etc).
    * @return
    */
   Set<String> getApplicationMethods();

   /**
    * Whether a class with a given name (.-separated) is an application class.
    * @param a class name (.-separated)
    * @return true if this is an application class, false otherwise (system- or 3rd lib class)
    */
   boolean isApplicationClass(String className);

}
