package nz.ac.wgtn.cornetto.output;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import nz.ac.wgtn.cornetto.SpecSource;
import nz.ac.wgtn.cornetto.http.ParameterSpec;
import nz.ac.wgtn.cornetto.jee.HttpRecord;
import nz.ac.wgtn.cornetto.jee.IssuePriority;
import nz.ac.wgtn.cornetto.jee.Loggers;
import org.apache.http.Header;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Abstract representation of an issue as an object.
 * This is a simple bean to be subclassed, the main purpose of this intermediate object representation is to use databinding
 * to export issues into JSON, and to parse recorded issues back for further analysis.
 * @author jens dietrich
 */

// ignore derived properties
@JsonIgnoreProperties(value = { "durationSinceStart" })
public abstract class Issue {


    private IssuePriority priority = null;
    private String method = null;
    private URI uri = null;
    private int statusCode = -1;
    private String statusReasonPhrase = null;
    private Map<String,String> requestHeaders = new LinkedHashMap<>();
    private Map<String,String> responseHeaders = new LinkedHashMap<>();
    private Map<String,String> parameters = new LinkedHashMap<>();
    private String responseEntity = null;
    private String requestEntity = null;
    private Set<String> requestGenerationProvenance = new HashSet<>();

    private long timestamp = -1;  // measured from when fuzzing started

    public long getTimestamp() {
        return timestamp;
    }

    // some additional convenient methods
    public String getDurationSinceStart () {
        return String.format("%d min, %d sec",
            TimeUnit.MILLISECONDS.toMinutes(timestamp),
            TimeUnit.MILLISECONDS.toSeconds(timestamp) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(timestamp))
        );
    }

    public int durationInMinsSinceStart() {
        return (int) TimeUnit.MILLISECONDS.toMinutes(timestamp);
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public IssuePriority getPriority() {
        return priority;
    }

    public void setPriority(IssuePriority priority) {
        this.priority = priority;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public URI getUri() {
        return uri;
    }

    public void setUri(URI uri) {
        this.uri = uri;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusReasonPhrase() {
        return statusReasonPhrase;
    }

    public void setStatusReasonPhrase(String statusReasonPhrase) {
        this.statusReasonPhrase = statusReasonPhrase;
    }

    public Map<String, String> getRequestHeaders() {
        return requestHeaders;
    }

    public void setRequestHeaders(Map<String, String> requestHeaders) {
        this.requestHeaders = requestHeaders;
    }

    public Map<String, String> getResponseHeaders() {
        return responseHeaders;
    }

    public void setResponseHeaders(Map<String, String> responseHeaders) {
        this.responseHeaders = responseHeaders;
    }

    public Map<String, String> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, String> parameters) {
        this.parameters = parameters;
    }

    public String getResponseEntity() {
        return responseEntity;
    }

    public void setResponseEntity(String responseEntity) {
        this.responseEntity = responseEntity;
    }

    public String getRequestEntity() {
        return requestEntity;
    }

    public void setRequestEntity(String requestEntity) {
        this.requestEntity = requestEntity;
    }

    public Set<String> getRequestGenerationProvenance() {
        return requestGenerationProvenance;
    }

    public void setRequestGenerationProvenance(Set<String> requestGenerationProvenance) {
        this.requestGenerationProvenance = requestGenerationProvenance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Issue issue = (Issue) o;
        return statusCode == issue.statusCode &&
                timestamp == issue.timestamp &&
                priority == issue.priority &&
                Objects.equals(method, issue.method) &&
                Objects.equals(uri, issue.uri) &&
                Objects.equals(statusReasonPhrase, issue.statusReasonPhrase) &&
                Objects.equals(requestHeaders, issue.requestHeaders) &&
                Objects.equals(responseHeaders, issue.responseHeaders) &&
                Objects.equals(parameters, issue.parameters) &&
                Objects.equals(responseEntity, issue.responseEntity) &&
                Objects.equals(requestEntity, issue.requestEntity) &&
                Objects.equals(requestGenerationProvenance, issue.requestGenerationProvenance);
    }

    @Override
    public int hashCode() {
        return Objects.hash(priority, method, uri, statusCode, statusReasonPhrase, requestHeaders, responseHeaders, parameters, responseEntity, requestEntity, requestGenerationProvenance, timestamp);
    }

    protected void initWith(HttpRecord httpRecord, String requestEntity, String responseEntity, IssuePriority priority) {
        this.setMethod(httpRecord.getRequestSpec().getMethodSpec().name());

        try {
            this.setUri(httpRecord.getRequestSpec().getUriSpec().toURI());
        }
        catch (URISyntaxException x) {
            Loggers.FUZZER.error("Error parsing uri",x);
        }

        this.statusCode = httpRecord.getResponse().getStatusLine().getStatusCode();
        this.statusReasonPhrase = httpRecord.getResponse().getStatusLine().getReasonPhrase();

        for (Header header:httpRecord.getRequest().getAllHeaders()) {
            this.requestHeaders.put(header.getName(),header.getValue());
        }
        for (ParameterSpec param:httpRecord.getRequestSpec().getParametersSpec().getParams()) {
            this.getParameters().put(param.getKey().getValue(),param.getValue().getValue());
        }
        this.requestEntity = requestEntity;
        this.responseEntity = responseEntity;

        for (Header header:httpRecord.getResponse().getAllHeaders()) {
            this.responseHeaders.put(header.getName(),header.getValue());
        }

        for (SpecSource specSource:httpRecord.getRequestSpec().getSources()) {
            this.requestGenerationProvenance.add(specSource.name());
        }

        this.priority = priority;
    }

}
