package nz.ac.wgtn.cornetto.authenticators;

import nz.ac.wgtn.cornetto.Profile;
import nz.ac.wgtn.cornetto.jee.Loggers;
import nz.ac.wgtn.cornetto.jee.Options;
import org.apache.http.Header;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * There are two authentication in Shopizer: customer login and admin login
 * @author Li Sui
 */
public class ShopizerAuthenticator implements Authenticator{
    private boolean needsAuthentication = true;
    //shopizer customer username&password
    public static final String USERNAME = "gg";
    public static final String USERPASSWORD = "gg";
    //shopizer admin username&password
    public static final String ADMINNAME="admin@shopizer.com";
    public static final String ADMINPASSWORD="password";

    @Override
    public boolean needsAuthentication() {
        return needsAuthentication;
    }

    @Override
    public void checkAuthenticationStatus(HttpRequest request, HttpResponse response, String responseEntityData) {
    }
    @Override
    public boolean authenticate(Profile profile, Options options, Function<HttpUriRequest, HttpResponse> httpClient, Consumer<List<BasicNameValuePair>> sessionHeaderConsumer) {
        return authenticate(profile, options,httpClient,sessionHeaderConsumer,true);
    }
    private boolean authenticate(Profile profile, Options options, Function<HttpUriRequest, HttpResponse> httpClient, Consumer<List<BasicNameValuePair>> sessionHeaderConsumer, boolean first) {
        //first time, register a customer.
        if (first) {
            try {
                Loggers.AUTHENTICATOR.info("Try to create a customer account " + USERNAME + " / " + USERPASSWORD);
                HttpPost registrationRequest = new HttpPost(Authenticator.getURI(profile,"shop/customer/register.html"));
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("billing.firstName", "kk"));
                params.add(new BasicNameValuePair("billing.lastName", "kk"));
                params.add(new BasicNameValuePair("billing.country", "AL"));
                params.add(new BasicNameValuePair("billing.stateProvince", "kk"));
                params.add(new BasicNameValuePair("emailAddress", "kk"));
                params.add(new BasicNameValuePair("userName", USERNAME));
                params.add(new BasicNameValuePair("password", USERPASSWORD));
                params.add(new BasicNameValuePair("checkPassword", USERPASSWORD));

                registrationRequest.setEntity(new UrlEncodedFormEntity(params));
                HttpResponse response = httpClient.apply(registrationRequest);
               // if (response.getStatusLine().getStatusCode() != 302) {
                 //   Loggers.AUTHENTICATOR.fatal("User registration has failed, response was: " + response.getStatusLine());
                //}
            } catch (Exception x) {
                Loggers.AUTHENTICATOR.error("Error registering Shopizer customer account to be used for fuzzing",x);
            }
        }
        boolean customerLoginSuccess=false;
        boolean adminLoginSuccess=false;
        //log in as a customer
        try {
            Loggers.AUTHENTICATOR.info("Log in as customer with username: " + USERNAME + " and password: " + USERPASSWORD);
            HttpPost customerLogin = new HttpPost(Authenticator.getURI(profile,"shop/customer/logon.html"));
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("userName", USERNAME));
            params.add(new BasicNameValuePair("password", USERPASSWORD));
            params.add(new BasicNameValuePair("storeCode", "DEFAULT"));
            customerLogin.setEntity(new UrlEncodedFormEntity(params));
            HttpResponse response =httpClient.apply(customerLogin);
            Loggers.AUTHENTICATOR.info("Authentication for customer login request status: " + response.getStatusLine());
            Loggers.AUTHENTICATOR.info("Authentication for customer login request headers:");
            for (Header header:response.getAllHeaders()) {
                Loggers.AUTHENTICATOR.info("\t" + header.getName() + " : " + header.getValue());
            }
            JSONObject responseObj= new JSONObject(EntityUtils.toString(response.getEntity()));
            boolean success = responseObj.getJSONObject("response").get("status").equals(0)
                    && responseObj.getJSONObject("response").get("userName").equals(USERNAME);
            if(success){
                customerLoginSuccess=true;
                Loggers.AUTHENTICATOR.info("customer logged in successfully!");
            }else {
                Loggers.AUTHENTICATOR.fatal("Authentication request for customer login has failed");
            }
        }catch (Exception x){
            Loggers.AUTHENTICATOR.fatal("Authentication request for customer login has failed",x);
        }



        //log in as admin
        try {
            Loggers.AUTHENTICATOR.info("Log in as admin with username: "+ADMINNAME+" and password: "+ADMINPASSWORD);
            //authentication url
            HttpPost adminLogin = new HttpPost(Authenticator.getURI(profile,"admin/performUserLogin"));
            adminLogin.setHeader("Content-Type","application/x-www-form-urlencoded");

            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("username", ADMINNAME));
            params.add(new BasicNameValuePair("password", ADMINPASSWORD));
            adminLogin.setEntity(new UrlEncodedFormEntity(params,"UTF-8"));
            HttpResponse response = httpClient.apply(adminLogin);

            Loggers.AUTHENTICATOR.info("Authentication for admin login request status: " + response.getStatusLine());
            Loggers.AUTHENTICATOR.info("Authentication for admin login request headers:");
            for (Header header:response.getAllHeaders()) {
                Loggers.AUTHENTICATOR.info("\t" + header.getName() + " : " + header.getValue());
            }
            boolean success = response.getStatusLine().getStatusCode()==302;
            if(success){
                adminLoginSuccess=true;
                Loggers.AUTHENTICATOR.info("admin logged in successfully!");
            }else {
                Loggers.AUTHENTICATOR.fatal("Authentication request for admin login has failed");
            }
        }catch (Exception x) {
            Loggers.AUTHENTICATOR.fatal("Authentication request for admin login has failed",x);
        }

        if(customerLoginSuccess && adminLoginSuccess){
            this.needsAuthentication=false;
            return true;
        }else{
            return false;
        }
    }

    @Override
    public boolean reAuthenticate(Profile profile, Options options, Function<HttpUriRequest, HttpResponse> httpClient, Consumer<List<BasicNameValuePair>> sessionHeaderConsumer) {
        return authenticate(profile, options, httpClient, sessionHeaderConsumer,false);
    }

    @Override
    public boolean isLogoutRequest(HttpUriRequest request) {
        if (request.getURI().toString().contains("/logout")) {
            Loggers.AUTHENTICATOR.warn("Potential logout request " + request.getMethod() + " " + request.getURI() );
            return true;
        }
        return false;
    }
}
