package nz.ac.wgtn.cornetto.authenticators;

import nz.ac.wgtn.cornetto.Profile;
import nz.ac.wgtn.cornetto.jee.Options;
import org.apache.http.Header;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Interface that manages authentication.
 * @author jens dietrich
 */
public interface Authenticator {

    boolean needsAuthentication();

    /**
     * Check the authentication status , for instance, by checking response headers.
     * This may result in changing the authentication status.
     * @param request
     * @param response
     * @param responseEntityData
     */
    void checkAuthenticationStatus(HttpRequest request , HttpResponse response, String responseEntityData);

    /**
     * Authenticate.
     * @param profile  the profile (to build URLs)
     * @param options
     * @param httpClient
     * @param sessionHeaderConsumer -- a consumer for additional session headers, such as crumbs (used by jenkins -- see https://stackoverflow.com/questions/44711696/jenkins-403-no-valid-crumb-was-included-in-the-request)
     * @return true if authentication succeeded, false otherwise
     */
    boolean authenticate(Profile profile, Options options, Function<HttpUriRequest,HttpResponse> httpClient, Consumer<List<BasicNameValuePair>> sessionHeaderConsumer);

    /**
     * Identify logout requests. This can be used by the fuzzer to avoid those requests (but potentially reducing coverage).
     * @param request
     * @return
     */
    boolean isLogoutRequest(HttpUriRequest request) ;

    /**
     * Re-authenticate. Defaults to authenticate.
     * @param profile  the profile (to build URLs)
     * @param options
     * @param httpClient
     * @param sessionHeaderConsumer -- a consumer for additional session headers, such as crumbs (used by jenkins -- see https://stackoverflow.com/questions/44711696/jenkins-403-no-valid-crumb-was-included-in-the-request)
     * @return true if authentication succeeded, false otherwise
     */
    default boolean reAuthenticate(Profile profile,Options options, Function<HttpUriRequest,HttpResponse> httpClient,Consumer<List<BasicNameValuePair>> sessionHeaderConsumer) {
        return this.authenticate(profile,options,httpClient,sessionHeaderConsumer);
    }

    // some useful methods to implement checks

    static boolean authenticationNeededStatus(HttpResponse response) {
        return response.getStatusLine().getStatusCode()==401 || response.getStatusLine().getStatusCode()==403;
    }

    static boolean hasHeader(HttpResponse response,String key,String value) {
        Header[] headers = response.getHeaders(key);
        for (Header header:headers) {
            if (header.getValue().equals(value)) {
                return true;
            }
        }
        return false;
    }

    static boolean hasHeaderSuchThat(HttpResponse response, String key, Predicate<String> valueCheck) {
        Header[] headers = response.getHeaders(key);
        for (Header header:headers) {
            if (valueCheck.test(header.getValue())) {
                return true;
            }
        }
        return false;
    }

    static URI getURI(Profile profile,String path) throws URISyntaxException {
        return new URIBuilder()
            .setScheme(profile.getProtocol().name())
            .setHost(profile.getHost())
            .setPort(profile.getPort())
            .setPath(profile.getPathFirstTokens().stream().collect(Collectors.joining("/"))+ "/" + path)
            .build();
    }

}
