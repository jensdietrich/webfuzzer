package nz.ac.wgtn.cornetto.authenticators;

import nz.ac.wgtn.cornetto.Profile;
import nz.ac.wgtn.cornetto.jee.Loggers;
import nz.ac.wgtn.cornetto.jee.Options;
import org.apache.http.Header;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * There are two authentication in pybbs: admin login & TODO: user login
 * @author Li Sui
 */
public class PybbsAuthenticator implements Authenticator{
    private boolean needsAuthentication = true;
    //pybbs admin username&password
    public static final String ADMINNAME="admin";
    public static final String ADMINPASSWORD="123123";
    @Override
    public boolean needsAuthentication() {
        return needsAuthentication;
    }

    @Override
    public void checkAuthenticationStatus(HttpRequest request, HttpResponse response, String responseEntityData) {

    }

    @Override
    public boolean authenticate(Profile profile, Options options, Function<HttpUriRequest, HttpResponse> httpClient, Consumer<List<BasicNameValuePair>> sessionHeaderConsumer) {
        //log in as admin
        try {
            Loggers.AUTHENTICATOR.info("Log in as admin with username: "+ADMINNAME+" and password: "+ADMINPASSWORD);
            //authentication url
            HttpPost adminLogin = new HttpPost(Authenticator.getURI(profile,"adminlogin"));
            adminLogin.setHeader("Content-Type","application/x-www-form-urlencoded");
            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("username", ADMINNAME));
            params.add(new BasicNameValuePair("password", ADMINPASSWORD));
            //TODO:CAPTCHA seems not working?
            params.add(new BasicNameValuePair("code", "fake"));
            params.add(new BasicNameValuePair("rememberMe","1"));
            adminLogin.setEntity(new UrlEncodedFormEntity(params));
            HttpResponse response = httpClient.apply(adminLogin);

            Loggers.AUTHENTICATOR.info("Authentication for admin login request status: " + response.getStatusLine());
            Loggers.AUTHENTICATOR.info("Authentication for admin login request headers:");
            for (Header header:response.getAllHeaders()) {
                Loggers.AUTHENTICATOR.info("\t" + header.getName() + " : " + header.getValue());
            }
            //TODO: why failure authentication still returns 302?
            boolean success = response.getStatusLine().getStatusCode()==302;
            if(success){
                this.needsAuthentication=false;
               return true;
            }else {
                Loggers.AUTHENTICATOR.fatal("Authentication request for admin login has failed");
                return false;
            }
        }catch (Exception x) {
            Loggers.AUTHENTICATOR.fatal("Authentication request for admin login has failed",x);
            return false;
        }
    }

    @Override
    public boolean isLogoutRequest(HttpUriRequest request) {
        if (request.getURI().toString().contains("/logout")) {
            Loggers.AUTHENTICATOR.warn("Potential logout request " + request.getMethod() + " " + request.getURI() );
            return true;
        }
        return false;
    }

    @Override
    public boolean reAuthenticate(Profile profile, Options options, Function<HttpUriRequest, HttpResponse> httpClient, Consumer<List<BasicNameValuePair>> sessionHeaderConsumer) {
        return Authenticator.super.reAuthenticate(profile, options, httpClient, sessionHeaderConsumer);
    }
}
