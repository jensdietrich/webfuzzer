package nz.ac.wgtn.cornetto;

import nz.ac.wgtn.cornetto.http.RequestSpec;

/**
 * Default history implementation based on  a  guava cache with max size.
 * @author jens dietrich
 */
public interface History {

    /**
     * Return true if this spec hasn't been seen yet.
     * This can be implemented using a probabilistic data structure.
     * @param spec
     * @return
     */
    boolean isNew(RequestSpec spec);

    /**
     * Add a spec.
     * @param spec
     */
    void keep(RequestSpec spec);

    /**
     * Get the count of duplicates that have been rejected.
     * @return
     */
    long getDuplicatesDetectedCount();

}
