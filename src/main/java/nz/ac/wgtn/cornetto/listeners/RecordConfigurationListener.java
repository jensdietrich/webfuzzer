package nz.ac.wgtn.cornetto.listeners;


import nz.ac.wgtn.cornetto.jee.Loggers;
import nz.ac.wgtn.cornetto.jee.Options;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Records the configuration used to run experiments -- all fuzzer options, as well as system properties.
 * This is useful to reproduce events.
 * @author jens dietrich
 */
public class RecordConfigurationListener extends AbstractFuzzerListener {


    public static final String fileName = "configuration.properties";

    @Override
    public void fuzzingStarts(Options options) {
        super.fuzzingStarts(options);

        File f = new File(outputRootFolder, fileName);
        try (PrintWriter out = new PrintWriter(new FileWriter(f))) {

            out.println("# Program options");
            for (String key : options.getOptionNames()) {
                out.println(key + " = " + options.getStringOption(key));
            }
            out.println();
            out.println("# JVM options");
            for (String key : System.getProperties().stringPropertyNames()) {
                out.println(key + " = " + System.getProperty(key));
            }

            Loggers.FUZZER.info("Configuration used in fuzzing campaign written to " + f.getAbsolutePath());
        } catch (IOException x) {
            Loggers.FUZZER.error("Error writing configuration used in fuzzing campaign to " + f.getAbsolutePath(),x);
        }
    }
}
