package nz.ac.wgtn.cornetto.listeners;

import com.google.common.io.Files;
import nz.ac.wgtn.cornetto.Context;
import nz.ac.wgtn.cornetto.ExecutionPoint;
import nz.ac.wgtn.cornetto.commons.LogSystem;
import nz.ac.wgtn.cornetto.feedback.CoverageGraph;
import nz.ac.wgtn.cornetto.html.Form;
import nz.ac.wgtn.cornetto.http.RequestSpec;
import nz.ac.wgtn.cornetto.http.SpecProvenanceUtils;
import nz.ac.wgtn.cornetto.jee.IssuePriority;
import nz.ac.wgtn.cornetto.jee.Options;
import nz.ac.wgtn.cornetto.jee.ResponseWrapper;
import nz.ac.wgtn.cornetto.jee.war.MethodInvocation;
import nz.ac.wgtn.cornetto.output.InjectionVulnerability;
import nz.ac.wgtn.cornetto.output.XSSVulnerability;
import org.apache.http.HttpRequest;
import org.apache.log4j.Logger;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Records performance stats.
 * @author jens dietrich
 */
public class StatsRecorderFuzzerListener extends AbstractStatsRecorderFuzzerListener  {

    private boolean fileInitialised = false;
    private File statsFile =  null;
    private long delay = 0;
    private int snapshotInterval = 60_000;

    public StatsRecorderFuzzerListener(int snapshotInterval) {
        this.snapshotInterval = snapshotInterval;
    }

    private Timer timer = null;

    @Override
    public void fuzzingStarts(Options options) {
        super.fuzzingStarts(options);
        this.statsFile = new File(outputRootFolder,"stats.csv");
        statsFile.delete();
        try {
            Files.touch(statsFile);
        }
        catch (IOException x) {
            LOGGER.error("Error creating stats service file", x);
        }

        TimerTask task = new TimerTask() {
            @Override
            public void run() {

                // snapshot
                Map<Key,Integer> snapshot = new HashMap<>();
                synchronized (StatsRecorderFuzzerListener.this) {
                    snapshot.putAll(counts);
                    counts.clear();
                }

                try (PrintWriter writer = new PrintWriter(new FileWriter(statsFile,true))) {
                    if (!fileInitialised) {
                        String headerLine = Stream.of(Key.values())
                            .map(k -> k.name())
                            .map(n -> n.replace('_','-').toLowerCase())
                            .collect(Collectors.joining(CSV_SEP));
                        headerLine = headerLine + CSV_SEP + "timestamp";
                        writer.println(headerLine);
                        fileInitialised = true;
                    }

                    // coverage is only updated when the snapshot is taken
                    updateCoverage();

                    String nextRow = Stream.of(Key.values())
                        .map(k -> {
                            int v = snapshot.computeIfAbsent(k,k2 -> 0);
                            if (k==Key.COVERAGE_AVG_SET_SIZE) {
                                // scale
                                return ""+(((double)v)/100);
                            }
                            else {
                                return ""+v;
                            }
                        })
                        .collect(Collectors.joining(CSV_SEP));
                    writer.print(nextRow);
                    writer.print(CSV_SEP);
                    writer.print(DATE_FORMAT.format(new Date()));
                    writer.println();

                    LOGGER.info("Stats data appended to " + statsFile.getAbsolutePath());

                } catch (IOException e) {
                    LOGGER.error("Error in stats service", e);
                }


            }

        };
        this.timer = new Timer();
        timer.scheduleAtFixedRate(task,delay,snapshotInterval);

//        Thread thread = new Thread(task,"webfuzzer.stats-service");
//        thread.setPriority(Thread.MIN_PRIORITY);
//        thread.start();
        LOGGER.info("Stats service started");
    }

    @Override
    public void fuzzingEnds() {
        super.fuzzingEnds();
        this.timer.cancel();
    }

    @Override
    public String toString() {
        return this.getClass().getName() + "{" + "snapshotInterval=" + snapshotInterval + " ms}";
    }

}
