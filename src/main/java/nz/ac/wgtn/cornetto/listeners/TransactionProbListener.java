package nz.ac.wgtn.cornetto.listeners;

import nz.ac.wgtn.cornetto.http.RequestSpec;
import nz.ac.wgtn.cornetto.jee.Loggers;
import nz.ac.wgtn.cornetto.jee.Options;
import nz.ac.wgtn.cornetto.jee.ResponseWrapper;
import org.apache.http.HttpRequest;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Listener that periodically logs details of a http transaction.
 * @author Jens Dietrich
 */
public class TransactionProbListener extends AbstractFuzzerListener  {

    private static final AtomicInteger counter = new AtomicInteger(0);
    private static final int DEFAULT_LOG_FREQUENCY = 5_000;
    private int logFrequency = DEFAULT_LOG_FREQUENCY;


    @Override
    public void fuzzingStarts(Options options) {
        super.fuzzingStarts(options);
        this.logFrequency = options.getIntOption("probInterval",DEFAULT_LOG_FREQUENCY,i -> i>0);
    }

    @Override
    public void requestSent(RequestSpec requestSpec, HttpRequest request, ResponseWrapper response) {

        super.requestSent(requestSpec,request, response);
        int index = counter.incrementAndGet();
        if (index%logFrequency==0) {
            File file = new File(outputRootFolder, "prob-" + index + ".txt");
            try (PrintWriter out = new PrintWriter(new FileWriter(file))) {
                out.println("# request " + index);
                StringificationUtil.print(out,requestSpec,request,response);
            } catch (Exception x) {
                Loggers.FUZZER.error("Error exporting probe", x);
            }
        }
    }
}
