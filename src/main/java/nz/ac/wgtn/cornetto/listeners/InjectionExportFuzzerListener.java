package nz.ac.wgtn.cornetto.listeners;

import com.fasterxml.jackson.databind.ObjectMapper;
import nz.ac.wgtn.cornetto.jee.IssuePriority;
import nz.ac.wgtn.cornetto.jee.Loggers;
import nz.ac.wgtn.cornetto.jee.Options;
import nz.ac.wgtn.cornetto.output.InjectionVulnerability;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Exports detected XSS Vulnerabilities to json.
 * @author jens dietrich
 */
public class InjectionExportFuzzerListener extends AbstractFuzzerListener  {

    private static final AtomicInteger counter = new AtomicInteger(0);
    private int MAX_LOGS = 0;

    // can use IssuePriority.ZERO for debugging
    public static final IssuePriority MIN_REPORTED_PRIORITY = IssuePriority.MEDIUM;

    @Override
    public void fuzzingStarts(Options options) {
        super.fuzzingStarts(options);
        MAX_LOGS = options.getIntOption("maxInjectionsReported",Integer.MAX_VALUE,v -> v>=-1);
        if (MAX_LOGS==-1) MAX_LOGS = Integer.MAX_VALUE;
        Loggers.FUZZER.info("Upto " + MAX_LOGS + " injection vulnerabilities details will be reported");
    }

    @Override
    public void newInjectionDiscovered(InjectionVulnerability injection) {
        super.newInjectionDiscovered(injection);
        injection.setTimestamp(System.currentTimeMillis() - this.fuzzingStartsTimestamp);
        int index = counter.incrementAndGet();
        if (index <= MAX_LOGS) {
            IssuePriority priority = injection.getPriority();
            if (priority.compareTo(MIN_REPORTED_PRIORITY) >= 0) {
                File file = new File(outputRootFolder, "injection-" + priority + "-" + index + ".json");
                ObjectMapper mapper = new ObjectMapper();
                try {
                    mapper.writerWithDefaultPrettyPrinter().writeValue(file, injection);
                } catch (IOException x) {
                    Loggers.RESULTS_INJECTION.error("Error exporting injection to " + file.getAbsolutePath(), x);
                }
            }
        }
    }
}
