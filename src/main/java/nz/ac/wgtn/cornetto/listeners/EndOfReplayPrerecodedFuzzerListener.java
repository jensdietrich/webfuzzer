package nz.ac.wgtn.cornetto.listeners;

import com.google.common.io.Files;
import nz.ac.wgtn.cornetto.Context;
import nz.ac.wgtn.cornetto.ExecutionPoint;
import nz.ac.wgtn.cornetto.feedback.CoverageGraph;
import nz.ac.wgtn.cornetto.html.Form;
import nz.ac.wgtn.cornetto.http.RequestSpec;
import nz.ac.wgtn.cornetto.http.SpecProvenanceUtils;
import nz.ac.wgtn.cornetto.jee.IssuePriority;
import nz.ac.wgtn.cornetto.jee.Options;
import nz.ac.wgtn.cornetto.jee.ResponseWrapper;
import nz.ac.wgtn.cornetto.jee.war.MethodInvocation;
import nz.ac.wgtn.cornetto.output.InjectionVulnerability;
import nz.ac.wgtn.cornetto.output.XSSVulnerability;
import org.apache.http.HttpRequest;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Records performance stats at when the replay of all prerecorded requests is done.
 * @author jens dietrich
 */
public class EndOfReplayPrerecodedFuzzerListener extends AbstractStatsRecorderFuzzerListener  {

    private AtomicBoolean isPrerecordingMode = new AtomicBoolean(true);

    public EndOfReplayPrerecodedFuzzerListener() {
    }

    @Override
    public void replayOfPrecordedRequestDone() {
        super.replayOfPrecordedRequestDone();

        assert isPrerecordingMode.get();
        isPrerecordingMode.set(false);

        File statsFile = new File(outputRootFolder,"prerecorded-stats.csv");
        statsFile.delete();
        try {
            Files.touch(statsFile);
        }
        catch (IOException x) {
            LOGGER.error("Error creating stats service file", x);
        }

        Map<Key,Integer> snapshot = new HashMap<>();
        updateCoverage(); // needs to be pulled
        snapshot.putAll(counts);

        try (PrintWriter writer = new PrintWriter(new FileWriter(statsFile,true))) {

            String headerLine = Stream.of(Key.values())
                .map(k -> k.name())
                .map(n -> n.replace('_','-').toLowerCase())
                .collect(Collectors.joining(CSV_SEP));
            headerLine = headerLine + CSV_SEP + "timestamp";
            writer.println(headerLine);

            String nextRow = Stream.of(Key.values())
                .map(k -> {
                    int v = snapshot.computeIfAbsent(k,k2 -> 0);
                    if (k==Key.COVERAGE_AVG_SET_SIZE) {
                        // scale
                        return ""+(((double)v)/100);
                    }
                    else {
                        return ""+v;
                    }
                })
                .collect(Collectors.joining(CSV_SEP));
            writer.print(nextRow);
            writer.print(CSV_SEP);
            writer.print(DATE_FORMAT.format(new Date()));
            writer.println();

            LOGGER.info("Stats data written to " + statsFile.getAbsolutePath());

        } catch (IOException e) {
            LOGGER.error("Error in prerecorded-stats service", e);
        }

        LOGGER.info("Prerecorded-stats service started");
    }


    @Override
    public synchronized void requestRejected(RequestSpec request) {
        if (isPrerecordingMode.get()) {
            super.requestRejected(request);
        }
    }

    @Override
    public void requestSent(RequestSpec requestSpec, HttpRequest request, ResponseWrapper response) {
        if (isPrerecordingMode.get()) {
            super.requestSent(requestSpec,request,response);
        }
    }

    @Override
    public synchronized void feedbackRequestSent(String ticketId, int statusCode) {
        if (isPrerecordingMode.get()) {
            super.feedbackRequestSent(ticketId,statusCode);
        }
    }

    @Override
    public synchronized void newExecutionPointsDiscovered(RequestSpec requestSpec, Set<ExecutionPoint> executionPoints) {
        if (isPrerecordingMode.get()) {
            super.newExecutionPointsDiscovered(requestSpec,executionPoints);
        }
    }

    @Override
    public synchronized void newRequestParameterNamesDiscovered(RequestSpec requestSpec, Set<String> requestParameterNames) {
        if (isPrerecordingMode.get()) {
            super.newRequestParameterNamesDiscovered(requestSpec,requestParameterNames);
        }
    }

    @Override
    public synchronized void newInvocationsOfCriticalMethodDiscovered(RequestSpec requestSpec, String stackTrace) {
        if (isPrerecordingMode.get()) {
            super.newInvocationsOfCriticalMethodDiscovered(requestSpec,stackTrace);
        }
    }

    @Override
    public synchronized void newXSSDiscovered(XSSVulnerability xssVulnerability) {
        if (isPrerecordingMode.get()) {
            super.newXSSDiscovered(xssVulnerability);
        }
    }

    @Override
    public synchronized void newFormsDiscovered(RequestSpec requestSpec, Set<Form> forms) {
        if (isPrerecordingMode.get()) {
            super.newFormsDiscovered(requestSpec,forms);
        }
    }

    @Override
    public void newInjectionDiscovered(InjectionVulnerability injection) {
        if (isPrerecordingMode.get()) {
            super.newInjectionDiscovered(injection);
        }
    }

    @Override
    public String toString() {
        return this.getClass().getName() + "{}";
    }

    @Override
    public void connectionErrorDetected(HttpRequest request, ConnectionErrorType errorType) {
        if (isPrerecordingMode.get()) {
            super.connectionErrorDetected(request,errorType);
        }
    }
}
