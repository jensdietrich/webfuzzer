package nz.ac.wgtn.cornetto.listeners;

import nz.ac.wgtn.cornetto.http.RequestSpec;
import nz.ac.wgtn.cornetto.jee.ResponseWrapper;
import org.apache.http.HttpRequest;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Listener that periodically triggers garbage collection.
 * This should not be necessary, but see https://bitbucket.org/jensdietrich/webfuzzer/issues/19/potenial-memory-leak-when-fuzzing-shopizer
 * @author jens dietrich
 */
public class GCFuzzerListener extends AbstractFuzzerListener {
    private AtomicInteger counter = new AtomicInteger(0);

    @Override
    public void requestSent(RequestSpec requestSpec, HttpRequest request, ResponseWrapper response) {
        super.requestSent(requestSpec, request, response);
        if (counter.incrementAndGet()%1_000==0) {
            System.gc();
        }
    }
}
