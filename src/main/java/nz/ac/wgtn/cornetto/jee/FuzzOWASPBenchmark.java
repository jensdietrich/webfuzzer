package nz.ac.wgtn.cornetto.jee;

import com.google.common.base.Preconditions;
import nz.ac.wgtn.cornetto.Context;
import nz.ac.wgtn.cornetto.DefaultDynamicModel;
import nz.ac.wgtn.cornetto.Profile;
import nz.ac.wgtn.cornetto.authenticators.NOPAuthenticator;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;

import java.io.File;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FuzzOWASPBenchmark extends AbstractFuzzer{
    // private AtomicInteger duplicateCounter = new AtomicInteger();
    private static int logProgressInterval = 100;

    static final File instrumentedWar = new File("examples/OWASP-benchmark/benchmark.war");

    public static void main(String[] args) throws Exception {

        Preconditions.checkState(instrumentedWar.exists());

        org.apache.commons.cli.Options cliOptions = new org.apache.commons.cli.Options()
                .addOption("logInterval", true, "The interval to report progress using logging (optional, default is " + logProgressInterval + ")")
                .addOption("trials", true, "The max number of generated requests (default is " + DEFAULT_MAX_TRIALS + ")")
                .addOption("campaignLength",true,"The max runtime of a fuzzing campaign, in minutes (default is " + DEFAULT_CAMPAIGN_LENGTH + ")")
                .addOption("randomseed", true, "Control randomness for the fuzzing run")
                .addOption("httpKeepAlive", true, "Set http client to use persistent connections")
                .addOption("requestgenerationthreads",true,"The number of thread used to generate requests")
                .addOption("requestexecutionthreads",true,"The number of thread used to execute requests")
                .addOption("responseevaluationthreads",true,"The number of thread used to evaluate responses")
                .addOption("requestgenerationthreads",true,"The number of thread used to generate requests")
                .addOption("requestexecutionthreads",true,"The number of thread used to execute requests")
                .addOption("responseevaluationthreads",true,"The number of thread used to evaluate responses")
                .addOption("requestqueuesize",true,"The size of the request queue")
                .addOption("responsequeuesize",true,"The size of the response queue")
                .addOption("requesttimeout",true,"The http request timeout in s")
                .addOption("probInterval",true,"The interval to log probs (request/response details)")
                .addOption("outputRootFolder",true,"The output root folder")
                .addOption("max500Reported",true,"The max number of 500 errors that will be reported")
                .addOption("maxXSSReported",true,"The max number of XSS vulnerabilities that will be reported")
                .addOption("maxInjectionsReported",true,"The max number of injection flaws that will be reported")
                .addOption("replayHarDir",true,"The dir that contains har files to be replayed")
                .addOption("subStringMatching",true,"enable substring matching for xss detection")
                .addOption("reportAllXSS",true,"enable to report all level of issues in xss")
                ;

        System.out.println(Stream.of(args).collect(Collectors.joining(" ")));
        CommandLine cmd = new DefaultParser().parse(cliOptions, args);

        FuzzOWASPBenchmark fuzzer = new FuzzOWASPBenchmark();
        Options options = Options.fromCLIArguments(cmd);

        fuzzer.authenticator = new NOPAuthenticator();

        fuzzer.fuzz(options);
    }


    @Override
    protected void init(Options options) throws Exception {
        super.init(options);

        Profile profile = new Profile.WebApplicationProfile("localhost",8080,"benchmark");
        Context context = new Context(
                profile,
                new JEEStaticModel(instrumentedWar, s->s.startsWith("org.owasp.benchmark"),profile),  // callgraph not used in example
                new DefaultDynamicModel()
        );
        context.setApplicationPackagePrefixes("org.owasp.benchmark");
        context.install(); // will be referenced by generators as singleton

    }

    @Override
    protected void deploy(Options options) throws Exception {

        Loggers.FUZZER.warn("Deployment of owasp benchmark server is manually - start server by cd to owasp benchmark repository");
        Loggers.FUZZER.warn("\trun: ./runBenchmark.sh");
        startTime = System.currentTimeMillis();
    }

    @Override
    protected void undeploy(Options options) throws Exception {
        // nothing to do here
    }
}