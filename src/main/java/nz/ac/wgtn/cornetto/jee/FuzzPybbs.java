package nz.ac.wgtn.cornetto.jee;

import com.google.common.base.Preconditions;
import nz.ac.wgtn.cornetto.Context;
import nz.ac.wgtn.cornetto.DefaultDynamicModel;
import nz.ac.wgtn.cornetto.Profile;
import nz.ac.wgtn.cornetto.authenticators.PybbsAuthenticator;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.http.message.BasicNameValuePair;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Adhoc fuzzing for pybbs.
 * @author jens dietrich
 */
public class FuzzPybbs extends AbstractFuzzer {
    // private AtomicInteger duplicateCounter = new AtomicInteger();
    private static int logProgressInterval = 100;

    static final File pybbsInstrumentedJar = new File("examples/pybbs/pybbs.jar");

    private static final File prerecordedRootFolder = null;

    public static void main(String[] args) throws Exception {

        Preconditions.checkState(pybbsInstrumentedJar.exists());

        org.apache.commons.cli.Options cliOptions = new org.apache.commons.cli.Options()
                .addOption("logInterval", true, "The interval to report progress using logging (optional, default is " + logProgressInterval + ")")
                .addOption("trials", true, "The max number of generated requests (default is " + DEFAULT_MAX_TRIALS + ")")
                .addOption("campaignLength",true,"The max runtime of a fuzzing campaign, in minutes (default is " + DEFAULT_CAMPAIGN_LENGTH + ")")
                .addOption("randomseed", true, "Control randomness for the fuzzing run")
                .addOption("httpKeepAlive", true, "Set http client to use persistent connections")
                .addOption("requestgenerationthreads",true,"The number of thread used to generate requests")
                .addOption("requestexecutionthreads",true,"The number of thread used to execute requests")
                .addOption("responseevaluationthreads",true,"The number of thread used to evaluate responses")
                .addOption("requestgenerationthreads",true,"The number of thread used to generate requests")
                .addOption("requestexecutionthreads",true,"The number of thread used to execute requests")
                .addOption("responseevaluationthreads",true,"The number of thread used to evaluate responses")
                .addOption("requestqueuesize",true,"The size of the request queue")
                .addOption("responsequeuesize",true,"The size of the response queue")
                .addOption("requesttimeout",true,"The http request timeout in s")
                .addOption("probInterval",true,"The interval to log probs (request/response details)")
                .addOption("outputRootFolder",true,"The output root folder")
                .addOption("max500Reported",true,"The max number of 500 errors that will be reported")
                .addOption("maxXSSReported",true,"The max number of XSS vulnerabilities that will be reported")
                .addOption("maxInjectionsReported",true,"The max number of injection flaws that will be reported")
                .addOption("replayHarDir",true,"The dir that contains har files to be replayed")
                ;

        System.out.println(Stream.of(args).collect(Collectors.joining(" ")));
        CommandLine cmd = new DefaultParser().parse(cliOptions, args);

        FuzzPybbs fuzzer = new FuzzPybbs();
        List<BasicNameValuePair> headers=new ArrayList<>();
        headers.add(new BasicNameValuePair("Accept","text/html"));
        fuzzer.extraHeaders= headers;
        Options options = Options.fromCLIArguments(cmd);

        fuzzer.authenticator = new PybbsAuthenticator();

        fuzzer.fuzz(options);

    }


    @Override
    protected void init(Options options) throws Exception {
        super.init(options);
        //no context path, map to ROOT
        Profile profile = new Profile.WebApplicationProfile("localhost",8080,"");
        Context context = new Context(
                profile,
                new JEEStaticModel(pybbsInstrumentedJar, s->s.startsWith("co.yiiu.pybbs"),profile),  // callgraph not used in example
                new DefaultDynamicModel()
        );
        context.setApplicationPackagePrefixes("co.yiiu.pybbs");
        context.install(); // will be referenced by generators as singleton
    }

    @Override
    protected void deploy(Options options) throws Exception {
        Loggers.FUZZER.warn("Deployment of pybbs server is manually - start server using this executable jar " + pybbsInstrumentedJar.getAbsolutePath() + " with JVM option \"-javaagent:\" " + new File("tools/aspectjweaver-1.9.6.jar").getAbsolutePath()+"\"");
        Loggers.FUZZER.warn("\tExample:");
        Loggers.FUZZER.warn("\tjava -javaagent:" + new File("tools/aspectjweaver-1.9.6.jar").getAbsolutePath() + " -jar " + pybbsInstrumentedJar.getAbsolutePath());
        startTime = System.currentTimeMillis();
    }

    @Override
    protected void undeploy(Options options) throws Exception {
        // nothing to do here
    }
}
