package nz.ac.wgtn.cornetto.jee;

import com.google.common.base.Preconditions;
import nz.ac.wgtn.cornetto.Profile;
import nz.ac.wgtn.cornetto.authenticators.Authenticator;
import nz.ac.wgtn.cornetto.http.RequestSpec;
import nz.ac.wgtn.cornetto.http.har.HarParser;
import org.apache.http.Header;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.message.BasicNameValuePair;
import java.io.File;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * An abstract authenticator where a script recorded in hjar format is used to run the authentication
 * request(s) necessary.
 * Such har files can be recorded using chrome developer tools.
 * There is a protected flag needsAuthentication to be maintained by subclasses.
 * @author jens dietrich
 */
public abstract class AbstractScriptedBasedAuthenticator implements Authenticator {

    protected boolean needsAuthentication = true;

    public abstract File getAuthScript();

    @Override
    public boolean authenticate(Profile profile, Options options, Function<HttpUriRequest,HttpResponse> httpClient, Consumer<List<BasicNameValuePair>> sessionHeadersConsumer) {
        File authScript = getAuthScript();
        Preconditions.checkState(authScript.exists());
        Loggers.AUTHENTICATOR.info("Using authentication script in " + authScript.getAbsolutePath());
        try {
            List<RequestSpec> authRequests = HarParser.parse(authScript);
            int counter = 0;
            for (RequestSpec rs : authRequests) {
                //Try sending each request in the list
                HttpUriRequest request = rs.toRequest();

                // TODO is server up ?  -- this is timing sensitive
                Loggers.AUTHENTICATOR.info("Sending authentication request (" + counter + ") : " + request);

                HttpResponse response = httpClient.apply(request);
                Loggers.AUTHENTICATOR.info("Authentication request (" + counter + ") returned " + (response == null ? "null" : response.getStatusLine()));
                counter++;

                if (response != null) {
                    // the Location header often provides an indication of whether authentication was successful
                    // e.g. it might be an error or login page if authentication fails, so lets log this
                    Header[] headers = response.getHeaders("Location");
                    if (headers != null) {
                        for (Header locationHeader : headers) {
                            String value = locationHeader.getValue();
                            String msg = "Authentication request (" + (counter - 1) + ") returned location header: " + value;
                            if (value.toLowerCase().contains("login") || value.toLowerCase().contains("error")) {
                                Loggers.AUTHENTICATOR.warn(msg);
                            } else {
                                Loggers.AUTHENTICATOR.info(msg);
                            }
                        }
                    }
                }

                if (response != null && response.getStatusLine().getStatusCode() == 503) {
                    Loggers.AUTHENTICATOR.info("Authentication request (" + (counter - 1) + ") returned 503, server might not yet be available, will wait and restart authentication");
                    try {
                        Thread.sleep(5000);
                        authenticate(profile,options,httpClient,sessionHeadersConsumer);
                    } catch (InterruptedException x) {
                    }
                }
            }

        }
        catch (Exception x) {
            Loggers.AUTHENTICATOR.warn("Error replaying authentication script");
            this.needsAuthentication = true;
            return false;
        }
        Loggers.AUTHENTICATOR.info("Finishing phase: authentication");
        this.needsAuthentication = false;
        return true;
    }

    @Override
    public boolean needsAuthentication() {
        return this.needsAuthentication;
    }

    @Override
    public void checkAuthenticationStatus(HttpRequest request, HttpResponse response, String responseEntityData) {
        this.needsAuthentication = !isAuthenticated(request,response,responseEntityData);
    }

    protected abstract boolean isAuthenticated(HttpRequest request, HttpResponse response, String responseEntityData) ;

}
