package nz.ac.wgtn.cornetto.jee;

import nz.ac.wgtn.cornetto.EntryPoint;
import nz.ac.wgtn.cornetto.Profile;
import nz.ac.wgtn.cornetto.StaticModel;
import nz.ac.wgtn.cornetto.html.Form;
import nz.ac.wgtn.cornetto.http.MethodSpec;
import nz.ac.wgtn.cornetto.jee.doop.CallgraphBuilder;
import nz.ac.wgtn.cornetto.jee.doop.PointstoBuilder;
import nz.ac.wgtn.cornetto.jee.war.AdvancedWarScopeModel;
import nz.ac.wgtn.cornetto.jee.war.StaticModelFromWar;

import java.io.File;
import java.net.URL;
import java.util.EnumSet;
import java.util.Set;
import java.util.function.Predicate;

/**
 * Composite pre-analysis model combining doop and war analysis.
 * @author jens dietrich
 */
public class JEEStaticModel implements StaticModel {

    private StaticModelFromWar warModel = null;

    public JEEStaticModel(File war,Predicate<String> isApplicationClass, Profile profile) throws Exception {
        this(war,new PointstoBuilder.Config(), new CallgraphBuilder.Config(), isApplicationClass,profile);
    }

    public JEEStaticModel(File war, PointstoBuilder.Config config1, CallgraphBuilder.Config config2, Predicate<String> isApplicationClass,Profile profile) throws Exception {
        warModel = new StaticModelFromWar(war,new AdvancedWarScopeModel(war,true,profile),profile,isApplicationClass);
//        Pointsto pointsto = doopPointsTo==null?null:new PointstoBuilder().build(doopPointsTo,config1);
//        Callgraph callgraph = doopCallgraph==null?null:new CallgraphBuilder().build(doopCallgraph,config2);
//        doopModel = new StaticModelFromDoop(callgraph,pointsto,isApplicationClass);
    }

    @Override
    public Set<String> getStringLiterals(EnumSet<Scope> scopes) {
        return warModel.getStringLiterals(scopes);
    }

    @Override
    public Set<Integer> getIntLiterals(EnumSet<Scope> scopes) {
        return warModel.getIntLiterals(scopes);
    }

    @Override
    public Set<String> getRequestParameterNames() {
        return warModel.getRequestParameterNames();
    }

    @Override
    public Set<String> getHeaderNames() {
        return warModel.getHeaderNames();
    }

    @Override
    public Set<EntryPoint> getEntryPoints() {
        return warModel.getEntryPoints();
    }

    @Override
    public Set<String> getEntryPointsURLPatterns() {
        return warModel.getEntryPointsURLPatterns();
    }

    @Override
    public Set<String> getReflectiveNames(EnumSet<Scope> scopes) {
        return warModel.getReflectiveNames(scopes);
    }

    @Override
    public Set<MethodSpec> getMethods() {
        return warModel.getMethods();
    }

    @Override
    public Set<Form> getForms() {
        return warModel.getForms();
    }

    @Override
    public Set<URL> getLinks() {
        return warModel.getLinks();
    }

    @Override
    public Set<String> getApplicationMethods() {
        return warModel.getApplicationMethods();
    }

    @Override
    public boolean isApplicationClass(String className) {
        return warModel.isApplicationClass(className);
    }
}
