package nz.ac.wgtn.cornetto.jee;

import com.google.common.base.Preconditions;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import java.io.IOException;

/**
 * Utility class to extract and cache the response entity data.
 * @author jens dietrich
 */
public class ResponseWrapper {
    private CloseableHttpResponse response = null;
    private String content = null;

    public ResponseWrapper(CloseableHttpResponse response) {
        Preconditions.checkNotNull(response);
        Preconditions.checkNotNull(response.getStatusLine());
        this.response = response;
    }

    public String getContent() {
        if (content == null) {
            try {
                this.content = EntityUtils.toString(response.getEntity());
            }
            catch (IOException x) {
                Loggers.FUZZER.warn("Error parsing response entity",x);
            }
        }
        return content;
    }

    public CloseableHttpResponse getResponse() {
        return response;
    }

    public int getStatusCode() {
        return this.response.getStatusLine().getStatusCode();
    }
}
