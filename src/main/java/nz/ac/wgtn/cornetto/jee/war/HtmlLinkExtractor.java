package nz.ac.wgtn.cornetto.jee.war;

import nz.ac.wgtn.cornetto.Profile;
import nz.ac.wgtn.cornetto.commons.LogSystem;
import nz.ac.wgtn.cornetto.html.HtmlParser;
import org.apache.log4j.Logger;

import java.io.File;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

/**
 * Collect links inside the war / jar.
 * @author jens dietrich
 */
public class HtmlLinkExtractor {

    public static final Logger LOGGER = LogSystem.getLogger("analyse-war-extract-links");

    public Set<URL> extractLinks(File war, Profile profile) throws Exception {
        Set<URL> links = new HashSet<>();

        // parse html
        new WarVisitor().visit(war,true,
            entry -> entry.getName().endsWith(".html") || entry.getName().endsWith(".htm") || entry.getName().endsWith(".xhtm") || entry.getName().endsWith(".xhtml") || entry.getName().endsWith(".jsp"),
            (name,in) -> {
                try {
                    links.addAll(HtmlParser.extractLinks(profile,in));
                }
                catch (Exception x) {
                    LOGGER.warn("Error parsing links in html resource " + name,x);
                }
            }
        );

        // parse adoc
        new WarVisitor().visit(war,true,
                entry -> entry.getName().endsWith(".adoc"),
                (name,in) -> {
                    try {

                    }
                    catch (Exception x) {
                        LOGGER.warn("Error parsing links in adoc resource " + name,x);
                    }
                }
        );

        LOGGER.info("Extracted links: " + links.size());
        return links;
    }

}
