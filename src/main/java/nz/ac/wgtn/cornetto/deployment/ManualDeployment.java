package nz.ac.wgtn.cornetto.deployment;

import nz.ac.wgtn.cornetto.commons.LogSystem;
import nz.ac.wgtn.cornetto.listeners.FuzzerListener;
import org.apache.log4j.Logger;
import javax.annotation.Nullable;
import java.io.File;
import java.util.Scanner;

/**
 * Deploy web app for fuzzing campaign manually.
 * @author jens dietrich
 */
public class ManualDeployment implements Deployment {

    private static final Logger LOGGER = LogSystem.getLogger("jee-fuzz-tomcat");

    private String contextName = null;
    @Override
    public boolean deploy(File war, File aspectJJar,String contextName) throws Exception {



        this.contextName = contextName;
        LOGGER.info("Manual deployment");
        LOGGER.info("\twar: " + war.getAbsolutePath());
        LOGGER.info("\taspectjJar: " + aspectJJar.getAbsolutePath());
        LOGGER.info("\tcontext: " + contextName);
        LOGGER.info("Manual deployment, the following steps must be performed:");
        LOGGER.info("The following steps must be performed:");
        LOGGER.info("1. deploy this war to server (e.g. copy into <server>/webapps , remove folder with application name if nnecessary):");
        LOGGER.info("\t" + war.getAbsolutePath());
        LOGGER.info("\t note that the war might have to be renamed to match " + contextName);
        LOGGER.info("2. ensure server starts with agent");
        LOGGER.info("\te.g. in Tomcat, add CATALINA_OPTS=-agentpath:" + aspectJJar.getAbsolutePath() + " to <TOMCAT_HOME>/catalina.sh");
        LOGGER.info("3. start the server");
        LOGGER.info("\te.g. in Tomcat, run <TOMCAT_HOME>/start.sh");
        LOGGER.info("4. check server availability");
        LOGGER.info("\te.g. curl localhost:8080/" + contextName);

        LOGGER.info("hit enter to continue");
        new Scanner(System.in).nextLine();

        return true;
    }

    @Override
    public void undeploy() throws Exception {
        LOGGER.info("Manual undeploymentent, the following steps must be performed:");
        LOGGER.info("1. stop the server");
        LOGGER.info("\te.g. in Tomcat, run <TOMCAT_HOME>/shutdown.sh");
        LOGGER.info("2. check server availability");
        LOGGER.info("\te.g. curl localhost:8080/" + contextName);

        LOGGER.info("hit enter to continue");
        new Scanner(System.in).nextLine();
    }

    @Override
    public String getHost() {
        return "localhost";
    }

    @Override
    public int getPort() {
        return 8080;
    }

    @Override
    public String getURL() {
        return getHost() + ':' + getPort() + contextName;
    }

    @Nullable
    @Override
    public FuzzerListener getMaintenanceListener() {
        return null;
    }
}
