package nz.ac.wgtn.cornetto.deployment;

import nz.ac.wgtn.cornetto.commons.LogSystem;
import nz.ac.wgtn.cornetto.listeners.FuzzerListener;
import org.apache.log4j.Logger;

import javax.annotation.Nullable;
import java.io.File;
import java.util.Scanner;

/**
 * Similar to manual deployment, but does not wait for user input, assuming that
 * deployment and undeployment will be scripted.
 * @author jens dietrich
 */
public class ExternalDeployment implements Deployment {

    private static final Logger LOGGER = LogSystem.getLogger("jee-fuzz-tomcat");
    private String contextName = null;
    @Override
    public boolean deploy(File war, File aspectJJar,String contextName) throws Exception {
        this.contextName = contextName;
        LOGGER.info("deployment of context " + contextName + " skipped (should be done externally (scripted)");
        return true;
    }

    @Override
    public void undeploy() throws Exception {
        LOGGER.info("undeployment of context " + contextName + " skipped (should be done externally (scripted)");
    }

    @Override
    public String getHost() {
        return "localhost";
    }

    @Override
    public int getPort() {
        return 8080;
    }

    @Override
    public String getURL() {
        return getHost() + ':' + getPort() + contextName;
    }

    @Nullable
    @Override
    public FuzzerListener getMaintenanceListener() {
        return null;
    }
}
