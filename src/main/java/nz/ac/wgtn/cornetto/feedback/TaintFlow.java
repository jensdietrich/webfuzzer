package nz.ac.wgtn.cornetto.feedback;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Part of a taint flow.
 * @author jens dietrich
 */
public class TaintFlow {
    private List<String> sourceStrings = null;
    private Map<String,String> arguments = null;
    private List<TaintedObject> provenance = null;

    public List<String> getSourceStrings() {
        return sourceStrings;
    }

    public void setSourceStrings(List<String> sourceStrings) {
        this.sourceStrings = sourceStrings;
    }

    public Map<String, String> getArguments() {
        return arguments;
    }

    public void setArguments(Map<String, String> arguments) {
        this.arguments = arguments;
    }

    public List<TaintedObject> getProvenance() {
        return provenance;
    }

    public void setProvenance(List<TaintedObject> provenance) {
        this.provenance = provenance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaintFlow taintFlow = (TaintFlow) o;
        return Objects.equals(sourceStrings, taintFlow.sourceStrings) &&
                Objects.equals(arguments, taintFlow.arguments) &&
                Objects.equals(provenance, taintFlow.provenance);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sourceStrings, arguments, provenance);
    }
}
