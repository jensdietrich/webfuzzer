package nz.ac.wgtn.cornetto.feedback;

import java.util.List;
import java.util.Objects;

/**
 * A taint propagation event.
 * @author jens dietrich
 */
public class TaintPropagationEvent {
    private String taintedBy = null;
    private String context = null;
    private List<String> sources;


    public String getTaintedBy() {
        return taintedBy;
    }

    public void setTaintedBy(String taintedBy) {
        this.taintedBy = taintedBy;
    }

    public  List<String> getSources() {
        return sources;
    }

    public void setSources( List<String> sources) {
        this.sources = sources;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaintPropagationEvent that = (TaintPropagationEvent) o;
        return
                Objects.equals(taintedBy, that.taintedBy) &&
                Objects.equals(sources, that.sources) &&
                Objects.equals(context, that.context);
    }

    @Override
    public int hashCode() {
        return Objects.hash(taintedBy, sources, context);
    }
}
