package nz.ac.wgtn.cornetto.postanalysis;

import java.io.File;

/**
 * Convenient script to re-run all analysis scripts after the data has changed.
 * Result files will be reported in the results/ folder.
 *
 * TODO : will re-reinitialise logging, resulting in multiple console appenders being created
 * @author jens dietrich
 */
public class AnalyseAllResults {

    public static final File RESULT_FOLDER = new File("results");
    public static void main (String[] args) throws Exception {
        if (!RESULT_FOLDER.exists()) {
            RESULT_FOLDER.mkdirs();
        }
        AnalyseErrorsInstanceCountsAndAggregations.main(new String[] {new File(RESULT_FOLDER,"error-analysis.tex").getAbsolutePath()});
        AnalyseXSSInstanceCountsAndAggregations.main(new String[] {new File(RESULT_FOLDER,"xss-analysis.tex").getAbsolutePath()});
        AnalyseInjectionsInstanceCountsAndAggregations.main(new String[] {new File(RESULT_FOLDER,"injection-analysis.tex").getAbsolutePath()});
        AnalyseInjectionCallsites.main(new String[] {new File(RESULT_FOLDER,"injection-callsite-analysis.tex").getAbsolutePath()});
        // AnalyseXSSTaints.main(new String[] {new File(RESULT_FOLDER,"xss-taint-analysis.tex").getAbsolutePath()});
        AnalyseCoverage.main(new String[] {
                new File(RESULT_FOLDER,"coverage-analysis.tex").getAbsolutePath(),
                new File(RESULT_FOLDER,"coverage-convergence-analysis.tex").getAbsolutePath()});
        AnalyseThroughput.main(new String[] {new File(RESULT_FOLDER,"throughput-analysis.tex").getAbsolutePath()});
    }
}
