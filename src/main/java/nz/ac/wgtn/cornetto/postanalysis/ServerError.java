package nz.ac.wgtn.cornetto.postanalysis;

import java.util.Map;

public class ServerError {
    String method;
    String uri;
    Map<String,String> requestHeaders;
    Map<String,String> parameters;
    String responseEntity;

    public String getMethod() {
        return method;
    }

    public String getUri() {
        return uri;
    }

    public Map<String,String> getRequestHeaders() {
        return requestHeaders;
    }

    public Map<String, String> getParameters() {
        return parameters;
    }

    public String getResponseEntity() {
        return responseEntity;
    }
}
