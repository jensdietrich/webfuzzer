package nz.ac.wgtn.cornetto.postanalysis;
import com.google.gson.Gson;
import com.google.common.base.Preconditions;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

import java.io.File;
import java.io.FileReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

/**
 * reproduce server error to identify TPs (500)
 * requirement: deploy the web application on tomcat.
 *   args[0]=path to the folder that contains all server error logs (i.e. server-error-1.json)
 *   args[1]=context path (e.g. shopizer)
 *   args[2]=authentication uri (e.g. http://localhost:8080/shopizer/admin/performUserLogin)
 *   args[3]=userName&password(e.g. tom&123)
 * @author Li Sui
 */
public class ReproduceServerErrors {
    public static String inputDir;
    public static String contextPath;
    public static String authURI;
    public static String username;
    public static String password;
    public static void main(String[] args) throws Exception{
        Preconditions.checkArgument(args.length==4,"this script requires four arguments " +
                "-- args[0]=path to the folder that contains all server error logs (i.e. server-error-1.json)" +
                "-- args[1]=context path (e.g. shopizer)" +
                "-- args[2]=authentication uri (e.g. http://localhost:8080/shopizer/admin/performUserLogin)" +
                "-- args[3]=userName&password(e.g. tom&123)");

        inputDir= args[0];
        contextPath=args[1];
        authURI=args[2];
        username=args[3].split("&")[0];
        password=args[3].split("&")[1];

        System.out.println("Analysing: "+inputDir);
        System.out.println("context path: "+contextPath);
        System.out.println("authentication URI: "+authURI);
        System.out.println("username: "+username);
        System.out.println("password: "+password);

        File dir = new File(inputDir);
        List<File> files = (List<File>) FileUtils.listFiles(dir, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
        for(File f:files){
            if(f.getName().startsWith("server-error")){
                HttpClient client = HttpClients.custom().build();
                authentication(client);
                Gson gson = new Gson();
                ServerError error = gson.fromJson(new FileReader(f), ServerError.class);
                //because the fuzzer produced uri with "name".fuzz as context name, we have to change it to fit with manually deployed one.
                String uri="http://localhost:8080/"+contextPath+error.getUri().split(".fuzz")[1];
                //process POST request
                if(error.method.equals("POST")){
                    processPost(uri, error,client,f);
                }
                //process GET request
                if(error.method.equals("GET")){
                    processGet(uri, error,client,f);
                }
            }
        }
    }

    private static void processPost(String uri, ServerError error, HttpClient client , File f) throws Exception{
        HttpPost post = new HttpPost(uri);
        for(String header: error.getRequestHeaders().keySet()) {
            post.setHeader(header,error.getRequestHeaders().get(header));
        }
        List<NameValuePair> paramsList = new ArrayList<>();
        for(String param:error.getParameters().keySet()) {
            paramsList.add(new BasicNameValuePair(param,error.getParameters().get(param)));
        }
        post.setEntity(new UrlEncodedFormEntity(paramsList,"UTF-8"));
        HttpResponse response = client.execute(post);
        if(response.getStatusLine().getStatusCode()==500){
            //TODO: format results
            System.out.println("POST request: "+response.getStatusLine()+" at file:"+f.getName()+" request:"+uri);
        }
    }

    private static void processGet(String uri, ServerError error,HttpClient client,File f ) throws Exception{
        HttpGet get =new HttpGet(uri);
        for(String header: error.getRequestHeaders().keySet()) {
            get.setHeader(header,error.getRequestHeaders().get(header));
        }
        List<NameValuePair> paramsList = new ArrayList<>();
        for(String param:error.getParameters().keySet()) {
            paramsList.add(new BasicNameValuePair(param,error.getParameters().get(param)));
        }
        URI u = new URIBuilder(get.getURI()).addParameters(paramsList).build();
        get.setURI(u);
        HttpResponse response = client.execute(get);
        if(response.getStatusLine().getStatusCode()==500){
            //TODO: format results
            System.out.println("GET request: "+response.getStatusLine()+" at file:"+f.getName()+" request:"+uri);
        }
    }


    private static void authentication(HttpClient client) throws Exception{
        HttpUriRequest request = RequestBuilder.post()
                .setUri(authURI)
                .setHeader(HttpHeaders.CONTENT_TYPE, "application/x-www-form-urlencoded")
                .addParameter("username",username)
                .addParameter("password",password)
                .build();
        //TODO: check response for successful authentication.
        HttpResponse response = client.execute(request);
    }
}
