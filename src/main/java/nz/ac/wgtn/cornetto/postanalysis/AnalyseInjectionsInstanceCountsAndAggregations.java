package nz.ac.wgtn.cornetto.postanalysis;

import com.fasterxml.jackson.databind.ObjectMapper;
import nz.ac.wgtn.cornetto.output.InjectionVulnerability;
import java.io.File;
import java.util.Map;
import java.util.function.Function;

/**
 * Script to analyse recorded injection instances.
 * @author jens dietrich
 */
public class AnalyseInjectionsInstanceCountsAndAggregations extends AbstractAnalyseIssuesInstanceCountsAndAggregations<InjectionVulnerability> {

    private ObjectMapper mapper = new ObjectMapper();

    public static void main (String[] args) throws Exception {
        new AnalyseInjectionsInstanceCountsAndAggregations().analyse(args);
    }

    @Override
    public Map<String, Function<InjectionVulnerability, Object>> getAggregations() {
        return Aggregations.INJECTION_AGGREGATIONS;
    }

    @Override
    public String getTableCaption() {
        return "Average number of injection vulnerabilities recorded over a number of runs and aggregations using URI, top stack frame and URI+top stack frame";
    }

    @Override
    public String getTableLabel() {
        return "tab:results:injection";
    }

    @Override
    public boolean isSerialisedIssue(File f) {
        return f.getName().startsWith("injection-") && f.getName().endsWith(".json");
    }

    @Override
    public InjectionVulnerability readIssue(String dataset,File file) throws Exception {
        return mapper.readValue(file, InjectionVulnerability.class);
    }
}
