package nz.ac.wgtn.cornetto.postanalysis;

import nz.ac.wgtn.cornetto.output.Issue;
import nz.ac.wgtn.cornetto.output.StackTrace;

/**
 * API for utility to extract stacktraces from (the response entities of) issues.
 * @author jens dietrich
 */
public abstract class StackTraceInference<T extends Issue> {

    public abstract StackTrace extractStackTrace(T issue) throws Exception;

    public StackTrace parse(String trace) {

        // enable for debugging
        // System.out.println(trace);

        String[] traceLines = trace.split("\n");
        StackTrace stackTrace = new StackTrace();
        StackTrace root = stackTrace;

        for (int i=0;i<traceLines.length;i++) {
            String traceLine = traceLines[i];
            if (i==0) {
                String[] parts = traceLine.split(":");
                stackTrace.setExceptionType(parts[0].trim());
                // message is optional
                if (parts.length>1) {
                    stackTrace.setExceptionMessage(parts[1].trim());
                }
                else {
                    stackTrace.setExceptionMessage("");
                }
            }
            else {
                traceLine = traceLine.trim();
                if (traceLine.startsWith("at ")) {
                    // regular entry
                    traceLine = traceLine.substring(3).trim();
                    stackTrace.addEntry(traceLine);
                }
                else if (traceLine.startsWith("Caused by: ")) {
                    // cause
                    StackTrace cause = new StackTrace();
                    stackTrace.setCause(cause);
                    stackTrace = cause;

                    // first line
                    traceLine = traceLine.substring(11).trim();
                    String[] parts = traceLine.split(":");
                    stackTrace.setExceptionType(parts[0].trim());
                    // message is optional
                    if (parts.length>1) {
                        stackTrace.setExceptionMessage(parts[1].trim());
                    }
                    else {
                        stackTrace.setExceptionMessage("");
                    }
                }

            }
        }
        return root;

    }
}
