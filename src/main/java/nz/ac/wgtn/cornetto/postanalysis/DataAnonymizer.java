package nz.ac.wgtn.cornetto.postanalysis;

import com.google.common.base.Preconditions;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Script to replace strings identifying authors from text files.
 * @author jens dietrich
 */
public class DataAnonymizer {

    public static final Properties RULES = new Properties() {{
        put("jens","<anonymiseduser>");
        put("NZ","<anonymisedcountry>");
        put("nz.ac.wgtn","<anonymisedpackage>");
    }};

    private static Path SOURCE_BASE = null;
    private static Path TARGET_BASE = null;
    private static Map<String,Integer> REPLACEMENT_COUNTERS = new HashMap<>();
    private static int FILE_COUNTER = 0;
    private static int TRANSFORMED_FILE_COUNTER = 0;
    private static int EXCEPTION_COUNTER = 0;

    public static void main(String[] args) throws Exception {
        Preconditions.checkState(args.length==2);
        SOURCE_BASE = Paths.get(args[0]).toAbsolutePath();
        TARGET_BASE = Paths.get(args[1]).toAbsolutePath();

        Preconditions.checkState(!SOURCE_BASE.equals(TARGET_BASE));

        Files.walk(SOURCE_BASE)
            .filter(Files::isRegularFile)
            .filter(f -> f.toString().endsWith(".json") || f.toString().endsWith(".txt") || f.toString().endsWith(".properties") || f.toString().endsWith(".csv"))
            .forEach(f -> anonymise(f));


        System.out.println("Files visited: " + FILE_COUNTER);
        System.out.println("Files transformed: " + TRANSFORMED_FILE_COUNTER);
        System.out.println("Rules applied: ");
        System.out.println("Exceptions: " + EXCEPTION_COUNTER);
        for (String key:RULES.stringPropertyNames()) {
            System.out.println("\treplace \"" + key + "\":" +  REPLACEMENT_COUNTERS.computeIfAbsent(key, k-> 0));
        }
    }

    private static void anonymise(Path source) {
        Path pathRelative = SOURCE_BASE.relativize(source);
        System.out.println("Visiting " + pathRelative.toString() + " - " + FILE_COUNTER);
        File target = new File(TARGET_BASE.toFile(),pathRelative.toString());
        FILE_COUNTER = FILE_COUNTER+1;
        try {
            target.getParentFile().mkdirs();
            List<String> lines = Files.readAllLines(source);
            List<String> transformedLines = new ArrayList<>(lines.size());
            boolean fileTransformed = false;
            for (String line:lines) {
                String line2 = null;
                for (String key:RULES.stringPropertyNames()) {
                    line2 = line.replace(key,RULES.getProperty(key));
                    if (line2.length()!=line.length()) {
                        REPLACEMENT_COUNTERS.compute(key,(k, v) -> v==null?1:(v+1));
                        fileTransformed = true;
                    }
                    line=line2;
                }
                transformedLines.add(line);
            }
            if (fileTransformed) {
                TRANSFORMED_FILE_COUNTER = TRANSFORMED_FILE_COUNTER + 1;
                System.out.println("Transforming " + pathRelative.toString() + " - " + FILE_COUNTER);
            }

            Files.write(target.toPath(),transformedLines);


//            System.out.println("transforming");
//            System.out.println("\tsource: " + source.toString());
//            System.out.println("\ttarget: " + source.toString());
        }
        catch (Exception x) {
            EXCEPTION_COUNTER = EXCEPTION_COUNTER + 1;
            x.printStackTrace();
        }
    }
}
