package nz.ac.wgtn.cornetto.postanalysis;

import com.google.gson.Gson;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileReader;
import java.nio.charset.Charset;
import java.util.*;

/**
 * This class analysis xss and injection results from OWASP benchmark
 *  take look at src/main/resources/owaspbenchmark-expectedresults-1.2.csv, if you want analysis more categories for injection report.
 *
 * @author Li Sui
 */
public class OWASPBenchmarkResultAnalysis {
    //private final static File INPUT_DIR= new File("/home/lsui/projects/webfuzzer/experiment-data/results-OWASPBenchmark-10-Dec-91%");
    private static File INPUT_DIR;
    private final static File EXPECTED_RESULT_FILE=new File(OWASPBenchmarkResultAnalysis.class.getResource("/owaspbenchmark-expectedresults-1.2.csv").getFile());
    private static Map<String,String> TRUE_ALARM=new HashMap<>();
    private static Map<String,String> FALSE_ALARM=new HashMap<>();
    //see src/main/resources/owaspbenchmark-expectedresults-1.2.csv, to add more categories.
    private final static List<String> INJECTION_CATG= Arrays.asList("sqli");

    public static void main(String[] args) throws Exception{

        if(args.length==0){
            System.err.println("require an arg to specify the location of reports");
            return;
        }
        String inputDir=args[0];
        INPUT_DIR=new File(inputDir);

        readExpectedResults();
        System.out.println("report xss attack, the evaluation is based on the category in benchmark: xss");
        processXSS();
        System.out.println("--------------------------------------------------\n");
        System.out.println("report injection attack, the evaluation is based on the category in benchmark: "+INJECTION_CATG);
        processInjection();
    }

    private static void processInjection()throws Exception{
        Map<String,String> tps=new HashMap<>();
        Map<String,String> fps=new HashMap<>();
        for(File f: FileUtils.listFiles(INPUT_DIR,null,true)){
            if(f.getName().startsWith("injection")){
                Gson gson = new Gson();
                ServerError vulnerability =gson.fromJson(new FileReader(f), ServerError.class);
                String benchmarkURI=vulnerability.getUri();
                checkInjection(benchmarkURI,tps,TRUE_ALARM);
                checkInjection(benchmarkURI,fps,FALSE_ALARM);
            }
        }
        int totalTrueAlarm=0;
        for(String catg:INJECTION_CATG){
            for(String value: TRUE_ALARM.values()){
                if(value.equals(catg)){
                    totalTrueAlarm++;
                }
            }
        }
        int totalFalseAlarm=0;
        for(String catg:INJECTION_CATG){
            for(String value: FALSE_ALARM.values()){
                if(value.equals(catg)){
                    totalFalseAlarm++;
                }
            }
        }
        double noTPs=tps.keySet().size();
        System.out.println("# of TPS:"+noTPs+"/"+totalTrueAlarm);
        System.out.println("recall="+noTPs/Double.valueOf(totalTrueAlarm));

        double noFPs=fps.keySet().size();
        System.out.println("# of FPS:"+noFPs+"/"+totalFalseAlarm);
        System.out.println("FPS rate:"+noFPs/Double.valueOf(totalFalseAlarm));
        System.out.println("printing FPS set------");
        System.out.println(fps.keySet());
        Set<String> fns=new HashSet<>();
        for(String testName: TRUE_ALARM.keySet()){
            if(INJECTION_CATG.contains(TRUE_ALARM.get(testName))){
                boolean flag=true;
                for(String uril:tps.keySet()){
                    if(uril.contains(testName)){
                        flag=false;
                    }
                }
                if(flag){
                    fns.add(testName);
                }
            }
        }
        System.out.println("# of FNS:"+fns.size());
        System.out.println("printing FNS set------");
        System.out.println(fns);
    }

    private static void processXSS()throws Exception{
        Map<String,String> tps=new HashMap<>();
        Map<String,String> fps=new HashMap<>();
        for(File f: FileUtils.listFiles(INPUT_DIR,null,true)){
            if(f.getName().startsWith("xss")){
                Gson gson = new Gson();
                ServerError vulnerability =gson.fromJson(new FileReader(f), ServerError.class);
                String benchmarkURI=vulnerability.getUri();
                checkXSS(benchmarkURI,tps,TRUE_ALARM);
                checkXSS(benchmarkURI,fps,FALSE_ALARM);
            }
        }


        double noTPs=tps.keySet().size();
        System.out.println("# of TPS:"+noTPs+"/246");
        System.out.println("recall="+noTPs/246.00);

        double noFPs=fps.keySet().size();
        System.out.println("# of FPS:"+noFPs+"/209");
        System.out.println("FPS rate:"+noFPs/209.00);
        System.out.println("printing FPS set------");
        System.out.println(fps.keySet());
        Set<String> fns=new HashSet<>();
        for(String testName: TRUE_ALARM.keySet()){
            if(TRUE_ALARM.get(testName).equals("xss")){
                boolean flag=true;
                for(String uril:tps.keySet()){
                    if(uril.contains(testName)){
                        flag=false;
                    }
                }
                if(flag){
                    fns.add(testName);
                }
            }
        }
        System.out.println("# of FNS:"+fns.size());
        System.out.println("printing FNS set------");
        System.out.println(fns);

    }

    private static void checkXSS(String benchmarkURI, Map<String,String> result, Map<String,String> map){
        for(String testName: map.keySet()){
            if(benchmarkURI.contains(testName)&&map.get(testName).equals("xss")){
                result.put(benchmarkURI,TRUE_ALARM.get(testName));
                return;
            }
        }
    }

    private static void checkInjection(String benchmarkURI, Map<String,String> result, Map<String,String> map){
        for(String testName: map.keySet()){
            if(benchmarkURI.contains(testName)&&INJECTION_CATG.contains(map.get(testName))){
                result.put(benchmarkURI,TRUE_ALARM.get(testName));
                return;
            }
        }
    }


    private static void readExpectedResults() throws Exception{
        String expectedResult= FileUtils.readFileToString(EXPECTED_RESULT_FILE, Charset.defaultCharset());
        for(String line : expectedResult.split("\n")){
            if(line.startsWith("BenchmarkTest")){
                String catg=line.split(",")[1];
                String testName=line.split(",")[0];
                String isTP=line.split(",")[2];
                if(isTP.equals("true")){
                    TRUE_ALARM.put(testName,catg);
                }
                if(isTP.equals("false")){
                    FALSE_ALARM.put(testName,catg);
                }
            }
        }
    }
}
