package nz.ac.wgtn.cornetto.postanalysis;

import nz.ac.wgtn.cornetto.output.ServerError;
import nz.ac.wgtn.cornetto.output.StackTrace;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.safety.Whitelist;

import javax.print.Doc;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;

/**
 * parser for stacktraces found in shopizer responses. Note that the prettyprint in Jsoup needs to be disable to preserve line breaks
 * @author Li Sui
 */
public class InferErrorStackTracesFromShopizerResponses  extends StackTraceInference<ServerError> {
    @Override
    public StackTrace extractStackTrace(ServerError error) throws Exception {
        String responseEntity = error.getResponseEntity();
        Document document = Jsoup.parse(responseEntity);
        //disable pretty print to preserve line break
        document.outputSettings(new Document.OutputSettings().prettyPrint(false));
        //a very interesting typo found in the response HTML, it supposes to be "strong"
        String trace= document.getElementsByTag("srong").get(0).html();
        return parse(trace);
    }
}
