package nz.ac.wgtn.cornetto.postanalysis;

import com.google.common.base.Preconditions;
import com.google.common.io.Files;
import com.google.common.math.Stats;
import nz.ac.wgtn.cornetto.output.Issue;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.util.*;
import java.util.stream.Collectors;

import static nz.ac.wgtn.cornetto.postanalysis.Commons.*;

/**
 * Script to analyse throughput.
 * @author jens dietrich
 */
public class AnalyseThroughput<T extends Issue> {


    public Logger LOGGER = Logger.getLogger(this.getClass());

    public static void main(String[] args) {
        new AnalyseThroughput<>().analyse(args);
    }
    private void analyse(String[] args) {
        BasicConfigurator.configure();
        Preconditions.checkArgument(args.length == 1, "this script requires one argument -- the name of the output file (.tex)");
        File resultFile = new File(args[0]);

        Map<String, List<Integer>> throughputsByDataset = new LinkedHashMap<>();
        for (int i = 0; i < DataSets.DATA_SETS.size(); i++) {
            String dataset = DataSets.DATA_SETS.get(i);
            LOGGER.info("Analysing dataset " + dataset);
            List<Integer> throughputs = new ArrayList<>();
            for (int j = 0; j < DataSets.getResults(dataset).size(); j++) {
                File resultFolder = DataSets.getResults(dataset).get(j);
                LOGGER.info("Analysing dataset " + dataset + " , run: " + resultFolder);
                File statsFile = new File(resultFolder, "stats.csv");
                try {
                    List<String> lines = Files.readLines(statsFile, Charset.defaultCharset());
                    lines.remove(0); // remove header, and values from startup / warmup
                    lines.remove(0);
                    lines.remove(0);
                    List<Integer> throughputs2 = lines.stream()
                            .map(line -> line.split("\t"))
                            .map(tokens -> tokens[0])
                            .map(value -> Integer.parseInt(value))
                            .collect(Collectors.toList());
                    throughputs.addAll(throughputs2);
                } catch (Exception x) {
                    LOGGER.error("Error reading file: " + statsFile.getAbsolutePath(), x);
                }

            }
            throughputsByDataset.put(dataset, throughputs);
        }

        LOGGER.info("Producing report");
        try (PrintWriter out = new PrintWriter(new FileWriter(resultFile))) {
            addProvenance2Latex(out, AnalyseThroughput.class);

            out.println("\\begin{table}[h]");
            out.println("\\center");
            out.println("\\begin{tabular}{l r r r r}");
            // header
            out.println("\\hline");
            out.println("\\textbf{program} & \\textbf{avg} & \\textbf{stdev} & \\textbf{min} & \\textbf{max}");
            out.println(" \\\\ ");
            out.println("\\hline");
            for (String dataset : DataSets.DATA_SETS) {
                out.print(dataset);
                out.print(" & ");
                List<Integer> values = throughputsByDataset.get(dataset);
                Stats stats = Stats.of(values);
                out.print(DEFAULT_NUMBER_FORMAT.format(stats.mean()));
                out.print(" & ");
                out.print(DEFAULT_NUMBER_FORMAT.format(stats.populationStandardDeviation()));
                out.print(" & ");
                out.print(INT_FORMAT.format(stats.min()));
                out.print(" & ");
                out.print(INT_FORMAT.format(stats.max()));
                out.println(" \\\\ ");
            }
            out.println("\\hline");
            out.println("\\end{tabular}");
            out.println("\\caption{Fuzzer throughput observed (requests/min)}");
            out.println("\\label{tab:results:throughput}");
            out.println("\\end{table}");
        } catch (Exception e) {
            LOGGER.error(e);
        }
        LOGGER.info("Report written to: " + resultFile.getAbsolutePath());
    }
}