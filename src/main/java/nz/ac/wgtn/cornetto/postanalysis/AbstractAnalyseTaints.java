package nz.ac.wgtn.cornetto.postanalysis;

import com.google.common.base.Preconditions;
import nz.ac.wgtn.cornetto.output.Issue;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.*;

import static nz.ac.wgtn.cornetto.postanalysis.Commons.addProvenance2Latex;

/**
 * Abstract script to analyse recorded issues for tainted strings used. Useful for analysing injections and XSS.
 * @author jens dietrich
 */
public abstract class AbstractAnalyseTaints<T extends Issue>  {

    protected final static Logger LOGGER = Logger.getLogger("postanalysis");

    public abstract String getTaint(T issue);

    public abstract T readIssue(String dataset,File file) throws Exception;

    public abstract String getTableCaption();

    public abstract String getTableLabel();

    public abstract boolean isSerialisedIssue(File file) ;

    public void analyse(String[] args) {
        BasicConfigurator.configure();
        Preconditions.checkArgument(args.length==1,"this script requires one argument -- the name of the output file (.tex)");
        File resultFile = new File(args[0]);

        Map<String, Map<String, String>> instanceStatsByTaintByDataset = new LinkedHashMap<>();  // values are latex-formatted averages

        for (int i=0;i<DataSets.DATA_SETS.size();i++) {
            String dataset = DataSets.DATA_SETS.get(i);
            LOGGER.info("Analysing dataset " + dataset);
            Map<String,Integer>[] taintCounters = new Map[DataSets.getResults(dataset).size()]; // slot for each run
            for (int j=0;j<DataSets.getResults(dataset).size();j++) {
                File resultFolder = DataSets.getResults(dataset).get(j);
                LOGGER.info("Analysing dataset " + dataset + " , run: " + resultFolder);
                taintCounters[j] = new HashMap<>();
                for (File errorFile:resultFolder.listFiles(f -> isSerialisedIssue(f))) {
                    try {
                        T issue = readIssue(dataset,errorFile);
                        String taint = getTaint(issue);
                        taintCounters[j].compute(taint,(k,v) -> v==null?1:(v+1));
                    } catch (Exception x) {
                        LOGGER.error("Error reading serialized error", x);
                    }
                }
            };
            LOGGER.info("Aggregating results for dataset " + dataset);
            Map<String, String> instanceStatsByTaint = new HashMap<>();
            instanceStatsByTaintByDataset.put(dataset,instanceStatsByTaint);

            // collect all taints
            Set<String> taints = new HashSet<>();
            for (Map<String,Integer> taintCounter:taintCounters) {
                taints.addAll(taintCounter.keySet());
            }
            for (String taint:taints) {
                List<Integer> counts = new ArrayList<>();
                for (Map<String,Integer> taintCounter:taintCounters) {
                    int count = taintCounter.computeIfAbsent(taint,cs -> 0); // important: record missing value for correct stats
                    counts.add(count);
                }
                String value = Commons.means2Latex(counts);
                instanceStatsByTaint.put(taint,value);
            }
        }

        LOGGER.info("Producing report");

        // collect ALL taints across datasets
        Set<String> taints = new HashSet<>();
        for (Map<String, String> instanceStatsByTaint:instanceStatsByTaintByDataset.values()) {
            taints.addAll(instanceStatsByTaint.keySet());
        }

        LOGGER.info("" + taints.size() + " found across all data sets and runs");
        try (PrintWriter out = new PrintWriter(new FileWriter(resultFile))) {

            addProvenance2Latex(out,this);

            String columFormats = new String(new char[DataSets.DATA_SETS.size()]).replace("\0", " l");
            columFormats = "l" + columFormats;  // 1st column is method
            out.println("\\begin{table}[h]");
            out.println("\\center");
            out.println("\\begin{tabular}{" + columFormats + "}");

            // header
            out.println("\\hline");
            out.print("\\textbf{method invoked} & ");
            for (String dataset:DataSets.DATA_SETS) {
                out.print(dataset);
            }
            out.println(" \\\\ ");
            out.println("\\hline");
            out.println("\\hline");

            // body
            for (String taint:taints) {
                out.print(taint);
                for (String dataset:DataSets.DATA_SETS) {
                    out.print(" & ");
                    Map<String, String> instanceStatsByTaint = instanceStatsByTaintByDataset.get(dataset);
                    String value = instanceStatsByTaint.computeIfAbsent(taint, k -> "-");
                    out.print(value);
                }
                out.println(" \\\\ ");
            }
            out.println("\\hline");
            out.println("\\end{tabular}");
            out.println("\\label{" + getTableLabel() + "}");
            out.println("\\caption{" + getTableCaption() + "}");
            out.println("\\end{table}");

            LOGGER.info("Report written to: " + resultFile.getAbsolutePath());

        }
        catch (Exception ex) {
            LOGGER.error(ex);
        }



    }
}
