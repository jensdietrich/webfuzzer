package nz.ac.wgtn.cornetto.postanalysis;

import nz.ac.wgtn.cornetto.output.InjectionVulnerability;
import nz.ac.wgtn.cornetto.output.ServerError;
import nz.ac.wgtn.cornetto.output.XSSVulnerability;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * Defines aggregations to be used when results are being reported.
 * An aggregation assigns an issue to an object that defines an equivalence class.
 * The class is defined by te function kernel, i.e. if two issues are mapped to objects that are equal,
 * then they are by definition in the same class.
 * @author jens dietrich
 */
public class Aggregations {

    public static Map<String, Function<ServerError,Object>> SERVER_ERROR_AGGREGATIONS = new LinkedHashMap<>();

    public static Map<String, Function<XSSVulnerability,Object>> XSS_AGGREGATIONS = new LinkedHashMap<>();

    public static Map<String, Function<InjectionVulnerability,Object>> INJECTION_AGGREGATIONS = new LinkedHashMap<>();

    static {
        SERVER_ERROR_AGGREGATIONS.put("instances", st->st); // entire stack and cause(s)
        SERVER_ERROR_AGGREGATIONS.put("full",error -> new StackTraceSummary(error.getStacktrace(),StackTraceSummary.ALL));
        SERVER_ERROR_AGGREGATIONS.put("top 10",error -> new StackTraceSummary(error.getStacktrace(),10));
        //SERVER_ERROR_AGGREGATIONS.put("top 5",error -> new StackTraceSummary(error.getStacktrace(),5));
        SERVER_ERROR_AGGREGATIONS.put("top 3",error -> new StackTraceSummary(error.getStacktrace(),3));
        SERVER_ERROR_AGGREGATIONS.put("top",error -> new StackTraceSummary(error.getStacktrace(),1));

        XSS_AGGREGATIONS.put("instances", xss -> xss);
        XSS_AGGREGATIONS.put("uri", xss -> xss.getUri());
        XSS_AGGREGATIONS.put("taint", xss -> xss.getTaintedInput());
        XSS_AGGREGATIONS.put("uri+taint", xss -> Pair.of(xss.getUri(),xss.getTaintedInput()));


        INJECTION_AGGREGATIONS.put("instances", injection -> injection);
        INJECTION_AGGREGATIONS.put("uri", injection -> injection.getUri());
//        INJECTION_AGGREGATIONS.put("uri+taint", injection -> Pair.of(
//            injection.getUri(),injection.getRequestHeaders().get("WEBFUZZ-TAINTED-VALUES")
//        ));
        INJECTION_AGGREGATIONS.put("top", injection -> injection.getTaintFlowSpec().getStackTrace().get(0));
        INJECTION_AGGREGATIONS.put("uri+top", injection -> Pair.of(
            injection.getUri(),
            injection.getTaintFlowSpec().getStackTrace().get(0)
        ));
    }
}
