package nz.ac.wgtn.cornetto.xss;

import com.google.common.base.Preconditions;
import nz.ac.wgtn.cornetto.http.RequestSpec;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;

/**
 * Template to generate requests with possible XSS by replacing simple tokens.
 * @author jens dietrich
 */
public class XSSCandidate {
    private RequestSpec requestSpec = null;
    private String xssToken = null;
    private Set<String> xssDefaultReplacementTokens = null;
    private Set<String> xssAdvancedReplacementTokens = null;

    public XSSCandidate(RequestSpec requestSpec, String xssToken) {
        this.requestSpec = requestSpec;
        this.xssToken = xssToken;

        this.xssDefaultReplacementTokens = new HashSet<>();
        this.xssDefaultReplacementTokens.addAll(XSSTokens.DEFAULT);

        this.xssAdvancedReplacementTokens = new HashSet<>();
        this.xssAdvancedReplacementTokens.addAll(XSSTokens.ADVANCED);

    }

    public void tokenConsumed(String token) {
        if (this.xssDefaultReplacementTokens.contains(token)) {
            this.xssDefaultReplacementTokens.remove(token);
        }
        else if (this.xssAdvancedReplacementTokens.contains(token)) {
            this.xssAdvancedReplacementTokens.remove(token);
        }
        else {
            Preconditions.checkArgument(false,"Illegal xss replacemnt token: " + token);
        }
    }

    // this method guards getNextReplacementToken, should be used in a synced block
    public boolean hasMoreReplacementsTokens() {
        return !xssDefaultReplacementTokens.isEmpty() || !xssAdvancedReplacementTokens.isEmpty();
    }

    public String getNextReplacementToken() {
        Preconditions.checkState(hasMoreReplacementsTokens());
        if (!xssDefaultReplacementTokens.isEmpty()) {
            Iterator<String> iter = xssDefaultReplacementTokens.iterator();
            String token = iter.next();
            iter.remove();
            return token;
        }
        else if (!xssAdvancedReplacementTokens.isEmpty()) {
            Iterator<String> iter = xssAdvancedReplacementTokens.iterator();
            String token = iter.next();
            iter.remove();
            return token;
        }
        // unreachable
        return null;
    }

    public RequestSpec getRequestSpec() {
        return requestSpec;
    }

    public String getXSSToken() {
        return xssToken;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        XSSCandidate that = (XSSCandidate) o;
        return Objects.equals(requestSpec, that.requestSpec);
    }

    @Override
    public int hashCode() {
        return Objects.hash(requestSpec);
    }
}
