package nz.ac.wgtn.cornetto.xss;

import com.google.common.collect.Multimap;
import com.googlecode.concurrenttrees.common.CharSequences;
import com.googlecode.concurrenttrees.radix.node.concrete.DefaultCharSequenceNodeFactory;
import com.googlecode.concurrenttrees.solver.LCSubstringSolver;
import nz.ac.wgtn.cornetto.Context;
import nz.ac.wgtn.cornetto.http.RequestSpec;
import nz.ac.wgtn.cornetto.http.SpecProvenanceUtils;
import nz.ac.wgtn.cornetto.listeners.FuzzerListener;
import nz.ac.wgtn.cornetto.output.XSSVulnerability;
import nz.ac.wgtn.cornetto.jee.HttpRecord;
import nz.ac.wgtn.cornetto.jee.IssuePriority;
import nz.ac.wgtn.cornetto.jee.Loggers;
import nz.ac.wgtn.cornetto.jee.Options;

import java.util.Collection;

/**
 * Default implementation of a XSS detection engine.
 * @author jens dietrich
 */
public class DefaultXSSDetectionEngine implements XSSDetectionEngine {

    public static final int SUBSTRING_SIZE_THRESHOLD = 2;
    private boolean SUBSTRING_MATCHING=false;


    @Override
    public void init(Options options) {
        if(options.getBooleanOption("subStringMatching",false)){
            SUBSTRING_MATCHING=true;
            Loggers.FUZZER.info("sub-string matching enabled for xss detection");
        }
        // can implement passing of parameters here
    }

    @Override
    public void checkForXSS(HttpRecord record, String responseEntityContent, Collection<FuzzerListener> fuzzerListeners) {
        // only check normal responses by default
        if (record.getResponse().getStatusLine().getStatusCode() >= 400) return;

        try {
            Multimap<String, RequestSpec.RequestDataKind> requestData = SpecProvenanceUtils.extractTaintedParts(record.getRequestSpec());

            for (String token:requestData.keySet()) {


                boolean isExactMatch =  SUBSTRING_MATCHING ? matchTrackedString(responseEntityContent,token): responseEntityContent.contains(token);
                if (isExactMatch && XSSTokens.ALL.contains(token)) {
                    if (XSSTokens.SIMPLE.contains(token)) {
                        XSSCandidate xssCandidate = new XSSCandidate(record.getRequestSpec(),token);
                        XSSVulnerability vulnerability = XSSVulnerability.newFrom(record,null,responseEntityContent,token, IssuePriority.LOW) ;
                        fuzzerListeners.forEach(l -> l.newXSSDiscovered(vulnerability));
                        // report for further mutation to dynamic model
                        Context.getDefault().getDynamicModel().registerXSSCandidate(xssCandidate);
                    }
                    else {
                        XSSVulnerability vulnerability = XSSVulnerability.newFrom(record,null,responseEntityContent,token, IssuePriority.HIGH) ;
                        fuzzerListeners.forEach(l -> l.newXSSDiscovered(vulnerability));
                    }
                }
            }
        }
        catch (Exception x) {
            Loggers.FUZZER.info("Error processing http transactions to scan for XSS vulnerabilities",x );
        }
    }


    // can be used for approximate match
    public boolean matchTrackedString (String responseEntityContent,String token) {
        //payload length
        int m = token.length();
        //response length
        int n = responseEntityContent.length();
        int result=Math.abs(m-lCSubStr(token.toCharArray(), responseEntityContent.toCharArray(), m, n));

        return result<=SUBSTRING_SIZE_THRESHOLD;
        // to avoid java.lang.IllegalArgumentException: The document argument was zero-length in LCSubstringSolver
//        if (responseEntityContent.length()< SUBSTRING_SIZE_THRESHOLD || token.length() < SUBSTRING_SIZE_THRESHOLD) {
//            return false;
//        }
//        else {
//            LCSubstringSolver solver = new LCSubstringSolver(new DefaultCharSequenceNodeFactory());
//            solver.add(token);
//            solver.add(responseEntityContent);
//            String longestCommonSubstring = CharSequences.toString(solver.getLongestCommonSubstring());
//            return longestCommonSubstring.length() >= SUBSTRING_SIZE_THRESHOLD;
//        }
    }

    private int lCSubStr(char[] x, char[] y, int m, int n) {
        int LCStuff[][] = new int[m + 1][n + 1];
        int result = 0;

        for (int i = 0; i <= m; i++) {
            for (int j = 0; j <= n; j++) {
                if (i == 0 || j == 0)
                    LCStuff[i][j] = 0;
                else if (x[i - 1] == y[j - 1]) {
                    LCStuff[i][j]
                            = LCStuff[i - 1][j - 1] + 1;
                    result = Integer.max(result,
                            LCStuff[i][j]);
                } else
                    LCStuff[i][j] = 0;
            }
        }
        return result;
    }
}
