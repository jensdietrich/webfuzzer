package nz.ac.wgtn.cornetto.xss;

import java.net.URI;
import java.util.Objects;

/**
 * Data structure used to group together similar XSS Instances, to facilitate compact reporting (such as: report only a limited number of
 * instances in this group.
 * @author jens dietrich
 */
public class XSSInstanceGroup {

    private String xssToken = null;
    private URI uri = null;

    public XSSInstanceGroup(URI uri, String xssToken) {
        this.xssToken = xssToken;
        this.uri = uri;
    }

    public String getXssToken() {
        return xssToken;
    }

    public URI getUri() {
        return uri;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        XSSInstanceGroup that = (XSSInstanceGroup) o;
        return Objects.equals(xssToken, that.xssToken) &&
                Objects.equals(uri, that.uri);
    }

    @Override
    public int hashCode() {
        return Objects.hash(xssToken, uri);
    }

    @Override
    public String toString() {
        return "XSSInstanceGroup{" +
                "xssToken='" + xssToken + '\'' +
                ", uri=" + uri +
                '}';
    }
}
