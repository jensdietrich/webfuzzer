package nz.ac.wgtn.cornetto.spikes;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

import java.io.*;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

/**
 * Ad hoc script to analyse the usage of parser APIs in wars.
 * @author jens dietrich
 */
public class AnalyseJSONandXMLParserUsage {

    public static File WAR_ROOT = new File("examples");
    public static File RESULTS = new File("parser-callsites.csv");

    // used to remove callsites in xml libs
    public static Predicate<String> IS_PARSER_LIB = s -> {
        return s.contains("/json-")
                || s.contains("/jackson-")
                || s.contains("/gson-")
                || s.contains("/dom4j-")
                || s.contains("/jaxb-")
                || s.contains("/fastjson-")
                || s.contains("/xercesImpl-")
                || s.contains("/xalan-")
                || s.contains("/jdom-")
                || s.contains("/wstx-")
                || s.contains("/jaxen-");
    };

    private static Collection<ParserCallsiteDef> callsiteDefs = null;


    public static void main (String[] args) throws Exception {

        callsiteDefs = new ParserCallsiteDefReader().read();

        System.out.println("" + callsiteDefs.size() + " parser callsite defs imported");

        Comparator comparator = Comparator.comparing((ParserCallsite cs)->cs.getWar())
                .thenComparing(cs->cs.getContext())
                .thenComparing(cs->cs.getDataKind())
                .thenComparing(cs->cs.getApiName())
                .thenComparing(cs->cs.getApiKind())
                .thenComparing(cs->cs.getCallee())
                .thenComparing(cs->cs.getCaller());

        Set<ParserCallsite> callsites = new TreeSet<>(comparator);

        Files.walkFileTree(WAR_ROOT.toPath(), new FileVisitor<Path>() {
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                if (file.toFile().getName().endsWith(".war")) {
                    System.out.println("Visiting: " + file.toFile().getAbsolutePath());
                    try {
                        callsites.addAll(extractCallsites(file.toFile()));
                    }
                    catch (Exception x) {
                        x.printStackTrace();
                    }
                }
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                return FileVisitResult.CONTINUE;
            }
        });

        // export results
        try (PrintWriter out = new PrintWriter(new FileWriter(RESULTS))) {
            append(out,"war","context","data kind","api","api type","callee","caller");

            for (ParserCallsite cs:callsites) {
                append(out,cs.getWar(),cs.getContext(),cs.getDataKind().name(),cs.getApiName(),cs.getApiKind().name(),cs.getCallee(),cs.getCaller());
            }

            System.out.println("Results written to " + RESULTS.getAbsolutePath());
        }
        catch (Exception x) {
            x.printStackTrace();
        }
    }

    private static void append(PrintWriter out,String... values) {
        out.println(Stream.of(values).collect(Collectors.joining("\t")));
    }

    /**
     * Extracts callsites to parse API methods.
     * @param war
     * @return a set of callsites
     * @throws Exception
     */
    public static Set<ParserCallsite> extractCallsites (File war) throws Exception {
        Set<ParserCallsite> callsites = new HashSet<>();
        ZipFile zip = new ZipFile(war);
        Enumeration<? extends ZipEntry> en = zip.entries();
        while (en.hasMoreElements()) {
            ZipEntry e = en.nextElement();
            String name = e.getName();
            if (name.endsWith(".class")) {
                InputStream in = zip.getInputStream(e);
                analyseClassFile(war.getName(),"<classes>",in,callsites);
                in.close();
            }
            if ((name.startsWith("WEB-INF/lib/") || name.startsWith("BOOT-INF/lib/")) && name.endsWith(".jar") && !IS_PARSER_LIB.test(name)) {
                InputStream in = zip.getInputStream(e);
                ZipInputStream input = new ZipInputStream(in);
                ZipEntry e2 = null;
                while ((e2 = input.getNextEntry()) != null ) {
                    if (e2.getName().endsWith(".class")) {
                        //InputStream in2 = zip.getInputStream(e2);
                        analyseClassFile(war.getName(),name,input,callsites);
                        //in2.close();
                    }
                }
                input.close();
            }
        }
        return callsites;
    }


    private static ParserCallsite findCallsite (String calleeClass, String calleeMethod, String calleeDescriptor, String caller, String warFileName, String context) {
        calleeClass = calleeClass.replace('/','.');

        for (ParserCallsiteDef def:callsiteDefs) {
            if (def.matches(calleeClass,calleeMethod,calleeDescriptor)) {
                String callee = generateMethodIdentifier(calleeClass,calleeMethod,calleeDescriptor);
                return new ParserCallsite(warFileName,context,caller,callee,def.getParserType(),def.getFormat(),def.getName());
            }
        }

        return null;
    }

    private static String generateMethodIdentifier (String className, String methodName, String descriptor) {
        return className.replace('/','.') + "::" + methodName + descriptor;
    }

    private static void analyseClassFile(String warFileName, String context,InputStream in,Set<ParserCallsite> callsites) throws Exception {
        ClassVisitor bytecodeVisitor = new ClassVisitor(Opcodes.ASM8) {
            private String callerClass = null;
            @Override
            public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
                super.visit(version, access, name, signature, superName, interfaces);
                this.callerClass = name;
            }

            @Override
            public MethodVisitor visitMethod(int access, String methodName, String methodDescriptor, String signature, String[] exceptions) {
                String caller = generateMethodIdentifier(callerClass,methodName,methodDescriptor);
                return new MethodVisitor(Opcodes.ASM8) {
                    @Override
                    public void visitMethodInsn(int opcode, String owner, String name, String descriptor, boolean isInterface) {
                        ParserCallsite callsite = findCallsite(owner,name,descriptor,caller,warFileName,context);
                        if (callsite != null) {
                            callsites.add(callsite);
                        }
                    }
                };
            }
        };
        new ClassReader(in).accept(bytecodeVisitor, 0);
    }



}
