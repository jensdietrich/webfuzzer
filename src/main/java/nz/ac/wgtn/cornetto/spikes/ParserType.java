package nz.ac.wgtn.cornetto.spikes;

/**
 * Types of parser.
 * @author jens dietrich
 */
enum ParserType {DOM, PULL, PUSH, DATABIND, PATHQUERY, OTHER}
