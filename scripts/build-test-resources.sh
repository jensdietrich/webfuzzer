#!/usr/bin/env bash
  
#  build war files from maven projects in test resources
if [ ! -e "./src/test/resources" ] ; then
  echo "Usage: ./scripts/build-test-resources.sh //  run script from project root directory"
  exit  
fi

for dir in src/test/resources/*                                                    
do                                                                                 
if [ -d "${dir}" ] ; then
  if [ -e "${dir}/pom.xml" ] ; then
   echo "Building: ${dir}" 
   eval  cd ${dir}
   mvn clean package
   cd - 
  fi
fi
done



