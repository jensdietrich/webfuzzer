#!/bin/bash

## SCRIPT TO RUN FUZZING CAMPAIGNS FOR STRUCTS IN BATCH MODE
NUMBER_OF_EXPERIMENTS=3
CAMPAIGN_RUNTIME=480
HARDIR=""

echo "building preparer"
cd ./war-preparer
mvn clean package
echo "building fuzzer"
cd ..
mvn clean package

for ((i = 1; i <= ${NUMBER_OF_EXPERIMENTS}; i++)); do
  echo "iteration ${i}"
  if [ $# -eq 1 ]; then
    HARDIR=$1
    ant -buildfile run.xml -propertyfile campaigns/struts.properties -DcampaignLength=${CAMPAIGN_RUNTIME} -DoutputRootFolder=results-struts-${i} -DreplayHarDir=${HARDIR}
  else
    echo "running without har files"
    ant -buildfile run.xml -propertyfile campaigns/struts.properties -DcampaignLength=${CAMPAIGN_RUNTIME} -DoutputRootFolder=results-struts-${i}
  fi
  #zip results, remove folder to save space
  zip -r results/results-struts-${i}.zip results-struts-${i}
  rm results-struts-${i}/*
  rm -r results-struts-${i}
  echo "iteration ${i} done"
done

echo "done !"
