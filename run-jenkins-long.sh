#!/bin/bash

## SCRIPT TO RUN FUZZING CAMPAIGNS FOR WEBGOAT IN BATCH MODE
NUMBER_OF_EXPERIMENTS=5
# 24h campaign
CAMPAIGN_RUNTIME=1440

echo "buildig preparer"
cd ./war-preparer
mvn clean package
echo "building fuzzer"
cd ..
mvn clean package

ant -buildfile run.xml -propertyfile campaigns/jenkins.properties -DcampaignLength=${CAMPAIGN_RUNTIME} -DoutputRootFolder=results-jenkins-24

echo "done !"